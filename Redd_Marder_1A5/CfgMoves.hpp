

    class CfgMovesBasic
    {

        class DefaultDie;
        class ManActions
        {

            Redd_Marder_Driver = "Redd_Marder_Driver";
            Redd_Marder_Driver_Out = "Redd_Marder_Driver_Out";
            Redd_Marder_Commander = "Redd_Marder_Commander";
            Redd_Marder_Commander_Winkelspiegel = "Redd_Marder_Commander_Winkelspiegel";
            Redd_Marder_Gunner = "Redd_Marder_Gunner";
            Redd_Marder_Commander_Milan = "Redd_Marder_Commander_Milan";
            Redd_Marder_Commander_Out_1 = "Redd_Marder_Commander_Out_1";
            Redd_Marder_Commander_Out_2 = "Redd_Marder_Commander_Out_2";
            Redd_Marder_Passenger_Back = "Redd_Marder_Passenger_Back";
            Redd_Marder_Passenger = "Redd_Marder_Passenger";

            KIA_Redd_Marder_Driver = "KIA_Redd_Marder_Driver";
            KIA_Redd_Marder_Driver_Out = "KIA_Redd_Marder_Driver_Out";
            KIA_Redd_Marder_Commander = "KIA_Redd_Marder_Commander";
            KIA_Redd_Marder_Commander_Winkelspiegel = "KIA_Redd_Marder_Commander_Winkelspiegel";
            KIA_Redd_Marder_Gunner = "KIA_Redd_Marder_Gunner";
            KIA_Redd_Marder_Commander_Milan = "KIA_Redd_Marder_Commander_Milan";
            KIA_Redd_Marder_Commander_Out_1 = "KIA_Redd_Marder_Commander_Out_1";
            KIA_Redd_Marder_Commander_Out_2 = "KIA_Redd_Marder_Commander_Out_2";
            KIA_Redd_Marder_Passenger_Back = "KIA_Redd_Marder_Passenger_Back";
            KIA_Redd_Marder_Passenger = "KIA_Redd_Marder_Passenger";

        };

    };

    class CfgMovesMaleSdr: CfgMovesBasic
    {

        class States
        {

            class Crew;

            class Redd_Marder_Driver: Crew
            {

                file = "\Redd_Marder_1A5\anims\Redd_Marder_Driver.rtm";
                interpolateTo[] = {"KIA_Redd_Marder_Driver",1};
                ConnectTo[]={"KIA_Redd_Marder_Driver", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };

            class KIA_Redd_Marder_Driver: DefaultDie
		    {

                file = "\Redd_Marder_1A5\anims\KIA_Redd_Marder_Driver.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            class Redd_Marder_Driver_Out: Crew
            {

                file = "\Redd_Marder_1A5\anims\Redd_Marder_Driver_Out.rtm";
                interpolateTo[] = {"KIA_Redd_Marder_Driver_Out",1};
                ConnectTo[]={"KIA_Redd_Marder_Driver_Out", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };

            class KIA_Redd_Marder_Driver_Out: DefaultDie
		    {

                file = "\Redd_Marder_1A5\anims\KIA_Redd_Marder_Driver_Out.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            class Redd_Marder_Commander: Crew
            {

                file = "\Redd_Marder_1A5\anims\Redd_Marder_Commander.rtm";
                interpolateTo[] = {"KIA_Redd_Marder_Commander",1};
                ConnectTo[]={"KIA_Redd_Marder_Commander", 1};
                 
            };

            class KIA_Redd_Marder_Commander: DefaultDie
		    {

                file = "\Redd_Marder_1A5\anims\KIA_Redd_Marder_Commander.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            class Redd_Marder_Commander_Winkelspiegel: Crew
            {

                file = "\Redd_Marder_1A5\anims\Redd_Marder_Commander_Winkelspiegel.rtm";
                interpolateTo[] = {"KIA_Redd_Marder_Commander_Winkelspiegel",1};
                ConnectTo[]={"KIA_Redd_Marder_Commander_Winkelspiegel", 1};

            };

            class KIA_Redd_Marder_Commander_Winkelspiegel: DefaultDie
		    {

                file = "\Redd_Marder_1A5\anims\KIA_Redd_Marder_Commander_Winkelspiegel.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            class Redd_Marder_Gunner: Crew
            {

                file = "\Redd_Marder_1A5\anims\Redd_Marder_Gunner.rtm";
                interpolateTo[] = {"KIA_Redd_Marder_Gunner",1};
                ConnectTo[]={"KIA_Redd_Marder_Gunner", 1};

            };

            class KIA_Redd_Marder_Gunner: DefaultDie
		    {

                file = "\Redd_Marder_1A5\anims\KIA_Redd_Marder_Gunner.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            class Redd_Marder_Commander_Milan: Crew
            {

                file = "\Redd_Marder_1A5\anims\Redd_Marder_Commander_Milan.rtm";
                interpolateTo[] = {"KIA_Redd_Marder_Commander_Milan",1};
                ConnectTo[]={"KIA_Redd_Marder_Commander_Milan", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1};

            }; 

            class KIA_Redd_Marder_Commander_Milan: DefaultDie
		    {

                file = "\Redd_Marder_1A5\anims\KIA_Redd_Marder_Commander_Milan.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            class Redd_Marder_Commander_Out_1: Crew
            {

                file = "\Redd_Marder_1A5\anims\Redd_Marder_Commander_Out_1.rtm";
                interpolateTo[] = {"KIA_Redd_Marder_Commander_Out_1",1};
                ConnectTo[]={"KIA_Redd_Marder_Commander_Out_1", 1};

            }; 

            class KIA_Redd_Marder_Commander_Out_1: DefaultDie
		    {

                file = "\Redd_Marder_1A5\anims\KIA_Redd_Marder_Commander_Out_1.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            class Redd_Marder_Commander_Out_2: Crew
            {

                file = "\Redd_Marder_1A5\anims\Redd_Marder_Commander_Out_2.rtm";
                interpolateTo[] = {"KIA_Redd_Marder_Commander_Out_2",1};
                ConnectTo[]={"KIA_Redd_Marder_Commander_Out_2", 1};

            };

            class KIA_Redd_Marder_Commander_Out_2: DefaultDie
		    {

                file = "\Redd_Marder_1A5\anims\KIA_Redd_Marder_Commander_Out_2.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            class Redd_Marder_Passenger_Back: Crew
            {

                file = "\Redd_Marder_1A5\anims\Redd_Marder_Passenger_Back.rtm";
                interpolateTo[] = {"KIA_Redd_Marder_Passenger_Back",1};
                ConnectTo[]={"KIA_Redd_Marder_Passenger_Back", 1};
                canPullTrigger = 0;
                disableWeapons=1;
			    disableWeaponsLong=1;

            };

            class KIA_Redd_Marder_Passenger_Back: DefaultDie
		    {

                file = "\Redd_Marder_1A5\anims\KIA_Redd_Marder_Passenger_Back.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

             class Redd_Marder_Passenger: Crew
            {

                file = "\Redd_Marder_1A5\anims\Redd_Marder_Passenger.rtm";
                interpolateTo[] = {"KIA_Redd_Marder_Passenger",1};
                ConnectTo[]={"KIA_Redd_Marder_Passenger", 1};
                canPullTrigger = 0;
                disableWeapons=1;
			    disableWeaponsLong=1;

            };

            class KIA_Redd_Marder_Passenger: DefaultDie
		    {

                file = "\Redd_Marder_1A5\anims\KIA_Redd_Marder_Passenger.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

        };

    };