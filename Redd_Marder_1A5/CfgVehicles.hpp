
	/*	Vehicle classes	*/
	class Optics_Armored
	{
		
		class Wide;
		class Medium;
		class Narrow;
		
	};
	
	class Redd_Marder_Commander_Optics: Optics_Armored
	{
		
		class Wide: Wide
		{
			
			initFov = 0.4;
			maxFov = 0.4;
			minFov = 0.4;
			visionMode[] = {"Normal", "NVG", "Ti"};
			thermalMode[] = {2,3};
			gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_MK20";
			gunnerOpticsEffect[] = {};
						
		};
		
		class Medium: Medium
		{
			
			initFov = 0.08;
			maxFov = 0.08;
			minFov = 0.08;
			visionMode[] = {"Normal", "NVG", "Ti"};
			thermalMode[] = {2,3};
			gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_MK20";
			gunnerOpticsEffect[] = {};
			
		};
		class Narrow: Narrow 
		{
			
			initFov = 0.03;
			maxFov = 0.03;
			minFov = 0.03;
			visionMode[] = {"Normal", "NVG", "Ti"};
			thermalMode[] = {2,3};
			gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_MK20";
			gunnerOpticsEffect[] = {};
			
		};
		
	};
	
	class Redd_Marder_Milan_Optics: Optics_Armored
	{
		
		class Wide: Wide
		{
			
			initFov = 0.4;
			maxFov = 0.4;
			minFov = 0.4;
			visionMode[] = {"Normal", "NVG", "Ti"};
			thermalMode[] = {4,5};
			gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_Milan";
			gunnerOpticsEffect[] = {};
						
		};
		
		class Medium: Medium
		{
			
			initFov = 0.08;
			maxFov = 0.08;
			minFov = 0.08;
			visionMode[] = {"Normal", "NVG", "Ti"};
			thermalMode[] = {4,5};
			gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_Milan";
			gunnerOpticsEffect[] = {};
						
		};
		class Narrow: Narrow 
		{
			
			initFov = 0.03;
			maxFov = 0.03;
			minFov = 0.03;
			visionMode[] = {"Normal", "NVG", "Ti"};
			thermalMode[] = {4,5};
			gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_Milan";
			gunnerOpticsEffect[] = {};
						
		};
		
	};
	
	class Redd_Marder_Gunner_Optics: Optics_Armored
	{
		
		class Wide: Wide
		{
			
			initFov = 0.4;
			maxFov = 0.4;
			minFov = 0.4;
			visionMode[] = {"Normal", "NVG", "Ti"};
			thermalMode[] = {2,3};
			gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_MK20";
			gunnerOpticsEffect[] = {};
			
		};
		
		class Medium: Medium
		{
			
			initFov = 0.08;
			maxFov = 0.08;
			minFov = 0.08;
			visionMode[] = {"Normal", "NVG", "Ti"};
			thermalMode[] = {2,3};
			gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_MK20";
			gunnerOpticsEffect[] = {};
			
		};
		
		class Narrow: Narrow 
		{
			
			initFov = 0.03;
			maxFov = 0.03;
			minFov = 0.03;
			visionMode[] = {"Normal", "NVG", "Ti"};
			thermalMode[] = {2,3};
			gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_MK20";
			gunnerOpticsEffect[] = {};
			
		};
		
	};

	class CfgVehicles 
	{
		/*	A3 DEFAULT INHERITANCE TREE START */
		// Do not modify the inheritance tree, unless you want to alter game's internal configs, or REALLY know what you're doing.
		class LandVehicle;
		
		class Tank: LandVehicle 
		{
			
			class NewTurret;
			class Sounds;
			class HitPoints;
			
		};
		
		class Tank_F: Tank 
		{
			
			class Turrets 
			{
				
				class MainTurret:NewTurret 
				{
					
					class ViewOptics;
					class Turrets 
					{
						
						class CommanderOptics;
						
					};
					
				};
				
			};
			
			class EventHandlers;
			class AnimationSources;
			class ViewPilot;
			class ViewOptics;
			class ViewCargo;
			class HeadLimits;
			class CargoTurret;
			
			class HitPoints: HitPoints 
			{
				
				class HitHull;
				class HitFuel;
				class HitEngine;
				class HitLTrack;
				class HitRTrack;
				
			};
			
			class Sounds: Sounds 
			{
				
				class Engine;
				class Movement;
				
			};
			
		};
		/*	A3 DEFAULT INHERITANCE TREE END	*/
		
		/*	Base class	*/

		class APC_Tracked_01_base_F: Tank_F {};

		class Redd_Marder_1A5_base: APC_Tracked_01_base_F
		{
			
			#include "Sounds.hpp"
			#include "PhysX.hpp"
			#include "Pip.hpp"
			
			displayname = "$STR_Marder_1A5";
			author = "Redd";
			model = "\Redd_Marder_1A5\Redd_Marder_1A5";	
			editorCategory = "Redd_Vehicles";
			editorSubcategory = "Redd_Spz";
			transportSoldier = 3;
			smokeLauncherGrenadeCount = 6;
			smokeLauncherVelocity = 14;
			smokeLauncherOnTurret = 1;
			smokeLauncherAngle = 142.5;
			getInAction="GetInMedium";
			getOutAction="GetOutMedium";
			driverAction="Redd_Marder_Driver_Out";
			driverInAction="Redd_Marder_Driver";
			cargoAction[]={"Redd_Marder_Passenger"};
			proxyIndex = 1;
			cargoProxyIndexes[] = {1,2,3};
			armor=300;
			armorStructural = 6;
			cost=1000000;
			driverForceOptics = 0;
			dustFrontLeftPos = "wheel_1_3_bound";
			dustFrontRightPos = "wheel_2_3_bound";
			dustBackLeftPos = "wheel_1_7_bound";
			dustBackRightPos = "wheel_2_7_bound";
			viewDriverInExternal = 1;
			viewCargoInExternal = 1;
			lodTurnedIn = 1100; //Pilot
			lodTurnedOut = 1100; //Pilot
			enableManualFire = 0;
			insideSoundCoef = 0.9;
			attenuationEffectType = "TankAttenuation";
			driverOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_D";
			commanderCanSee = 31+32+14;
			threat[] = {1.0,0.6,0.6};
			driverCompartments = "Compartment1";
			cargoCompartments[] = {"Compartment2"};
			aggregateReflectors[] = {{"Left","Right"}};
			selectionBrakeLights = "zadni svetlo";
			selectionBackLights = "brzdove svetlo";
			driverLeftHandAnimName 	= "drivewheel";
		    driverRightHandAnimName = "drivewheel";
			memoryPointSupply = "pos cargo";
			TFAR_AdditionalLR_Turret[] = {{0,1}};
			forceHideDriver = 0;
			
			hiddenSelections[] = 
			{
				
				"Camo1",
				"Camo2",
				"zug1",
				"zug2",
				"plate_hinten_1",
				"plate_hinten_2",
				"plate_hinten_3",
				"plate_hinten_4",
				"plate_hinten_5",
				"plate_hinten_6",
				"plate_vorne_1",
				"plate_vorne_2",
				"plate_vorne_3",
				"plate_vorne_4",
				"plate_vorne_5",
				"plate_vorne_6",
				"Bataillon",
				"Kompanie",
				"Bataillon_hinten",
				"Kompanie_hinten"
				
			};
			
			// Damage textures
			class Damage 
			{
				tex[] = {};
				mat[] = 
				{

					"Redd_Marder_1A5\mats\Redd_Marder_1A5_Camo1.rvmat",
					"Redd_Marder_1A5\mats\Redd_Marder_1A5_Camo1_damage.rvmat",
					"Redd_Marder_1A5\mats\Redd_Marder_1A5_Camo1_destruct.rvmat"

				};
				
        	};

			class ViewOptics: ViewOptics 
			{
				
				initFov = 1;
				maxFov = 1;
				minFov = 1;
				visionMode[] = {"Normal","NVG"};
				
			};
			
			class Exhausts 
			{
				class Exhaust1 
				{
					
					position = "exhaust";
					direction = "exhaust_dir";
					effect = "ExhaustEffectTankBack";
					
				};
				
			};
			
			class Reflectors 
			{

				class Left 
				{
					
					color[] = {1900, 1800, 1700};
					ambient[] = {3, 3, 3};
					position = "Light_L";
					direction = "Light_L_end";
					hitpoint = "Light_L";
					selection = "Light_L";
					size = 1;
					innerAngle = 75;
					outerAngle = 120;
					coneFadeCoef = 10;
					intensity = 1;
					useFlare = 1;
					dayLight = 1;
					flareSize = 1;

					class Attenuation 
					{
						
						start = 1;
						constant = 0;
						linear = 0;
						quadratic = 0.25;
						hardLimitStart = 30;
						hardLimitEnd = 60;
						
					};
					
				};
				
				class Right: Left 
				{
					
					position = "Light_R";
					direction = "Light_R_end";
					hitpoint = "Light_R";
					selection = "Light_R";
					
				};

			};
			
			class HitPoints: HitPoints 
			{
				
				class HitHull: HitHull 
				{	
				
					armor=2;
					material=-1;
					name="telo";
					visual="Camo1";
					passThrough=0;
					minimalHit = 0.14;
					explosionShielding = 2.0;
					radius = 0.3;
					
				};

				class HitFuel: HitFuel
				{
					
					armor = 0.5;
					material = -1;
					name = "fuel";
					visual="Camo1";
					passThrough = 0;
					minimalHit = 0.1;
					explosionShielding = 0.4;
					radius = 0.3;
					
				};
				
				class HitEngine: HitEngine 
				{
					
					armor=1;
					material=-1;
					name="motor";
					visual="Camo1";
					passThrough=0;
					minimalHit = 0.24;
					explosionShielding = 1;
					radius = 0.3;
					
				};
				
				class HitLTrack: HitLTrack 
				{
					
					armor=1;
					material=-1;
					name="pas_L";
					visual="Camo1";
					passThrough=0;
					minimalHit = 0.08;
					explosionShielding = 1.44;
					radius = 0.25;
					
				};
				
				class HitRTrack: HitRTrack 
				{
					
					armor=1;
					material=-1;
					name="pas_P";
					visual="Camo1";
					passThrough=0;
					minimalHit = 0.08;
					explosionShielding = 1.44;
					radius = 0.25;
					
				};

			};

			class TransportMagazines {};

			class TransportWeapons {};
			
			class TransportBackpacks 
			{

				class _xx_B_AssaultPack_rgr
				{
					
					backpack = "B_AssaultPack_rgr";
					count = 1;
					
				};

				class _xx_milan_Bag
				{
					
					backpack = "Redd_Milan_Static_Bag";
					count = 1;
					
				};

				class _xx_milan_tripod
				{
					
					backpack = "Redd_Milan_Static_Tripod";
					count = 1;
					
				};
				
			};
			
			class TransportItems 
			{

				class _xx_Toolkit 
				{
				
					name = "Toolkit";
					count = 1;
				
				};
			
			};
			
			class Turrets: Turrets 
			{
				
				class MainTurret: MainTurret //[0]
				{
					
					class Turrets: Turrets 
					{
						
						class CommanderOptics: CommanderOptics //[0,0] Commander
						{
						
							turretCanSee = "CanSeeRadarC";
							gunnerForceOptics=0;
							gunnerOutForceOptics = 0;
							gunnerAction = "Redd_Marder_Commander_Out_1";
							gunnerInAction="Redd_Marder_Commander";
							gunnerOpticsEffect[] = {};
							gunnerOutOpticsModel = "";
							body = "commander_turret";
							gun = "commander_gun";
							animationSourceBody = "CommanderTurret_Source";
							animationSourceGun = "CommanderGun_Source";
							proxyIndex = 1;
							initElev = 0;
							minElev = -15;
							maxElev = 15;
							initTurn = 0;
							minTurn = -68;
							maxTurn = 68;
							inGunnerMayFire = 1;
							outGunnerMayFire = 1;
							viewGunnerInExternal = 1;
							hideProxyInCombat = 1;
							proxyType= "CPGunner";
							startEngine = 0;
							GunnerGetInAction="GetInHigh";
							GunnerGetOutAction="GetOutHigh";
							turretInfoType = "RscOptics_crows";
							canUseScanner = 0;
							allowTabLock = 0;
							ace_fcs_Enabled = 0;
							stabilizedInAxes = 3;
							maxHorizontalRotSpeed = 1.8;
							maxVerticalRotSpeed = 1.8;
							castGunnerShadow = 1;
							isPersonTurret =0;
							minOutElev = -45;
							maxOutElev = 45;
							initOutElev = 0;
							minOutTurn = -120;
							maxOutTurn = 80;
							initOutTurn = 0;
							lodTurnedIn = 1100; //Pilot
							lodTurnedOut = 1100; //Pilot
							soundAttenuationTurret = "TankAttenuation";
							primaryGunner = 0;
							primaryObserver = 1;
							commanding = 2;
							gunnerCompartments= "Compartment3";
							weapons[] = {"Redd_SmokeLauncher"};
							magazines[] = {"Redd_SmokeLauncherMag"};
							animationSourceHatch = "hatchCommander_Source";

							class ViewOptics: ViewOptics
							{
								
								visionMode[] = {"Normal", "NVG", "TI"};
								thermalMode[] = {2,3};
								initFov = 0.4;
								minFov = 0.08;
								maxFov = 0.03;
								
							};
							
							class OpticsIn: Redd_Marder_Commander_Optics 
							{

								class Wide: Wide {};

								class Medium: Medium {};

								class Narrow: Narrow {};
								
							};

							class HitPoints 
							{
								
								class HitTurret	
								{
									
									armor = 1;
									material = -1;
									name = "vezVelitele";
									visual="Camo1";
									passThrough = 0;
									minimalHit = 0.03;
									explosionShielding = 0.6;
									radius = 0.08;
									
								};
								
								class HitGun	
								{
									
									armor = 1;
									material = -1;
									name = "zbranVelitele";
									visual="Camo1";
									passThrough = 0;
									minimalHit = 0.03;
									explosionShielding = 0.6;
									radius = 0.08;
									
								};
								
							};
							
						};

						class CargoTurret_Links: NewTurret //[0,1] //Luke_links
						{

							gunnerInAction = "Redd_Marder_Passenger";
							gunnerName = "$STR_Dachluke_links";
							memoryPointsGetInGunner= "pos_hatch_3";
							memoryPointsGetInGunnerDir= "pos_hatch_3_dir";
							animationSourceHatch = "Hatch_left_Source";
							enabledByAnimationSource = "Hatch_left_rot";
							proxyIndex = 4;
							proxyType= "CPGunner";
							weapons[] = {};
							magazines[] = {};
							primaryGunner = 0;
							primaryObserver = 0;
							gunnerOpticsShowCursor = 0;
							body = "";
							gun = "";
							animationSourceBody = "";
							animationSourceGun = "";
							soundServo[] = {"", "db-50", 1.000000};
							startEngine = 0;
							hideWeaponsGunner = 0;
							GunnerGetInAction="GetInLow";
							GunnerGetOutAction="GetOutLow";
							gunnerAction  = "vehicle_turnout_1";
							isPersonTurret = 1;
							dontCreateAi = 1;
							commanding = -1;
							gunnerOpticsModel = "";
							gunnerOutOpticsColor[] = {0, 0, 0, 1};
							gunnerForceOptics = 0;
							gunnerOutForceOptics = 0;
							gunnerOutOpticsShowCursor = 0;
							memoryPointGunnerOptics = "";
							gunnerOpticsEffect[] = {};
							outGunnerMayFire = 1;
							inGunnerMayFire = 0;
							ejectDeadGunner  = 0; 
							lodTurnedIn = 1200; //Cargo
							lodTurnedOut = 1100; //Pilot
							allowLauncherOut = 0;
							allowLauncherIn = 0;
							soundAttenuationTurret = "TankAttenuation";
							disableSoundAttenuation = 0;
							viewGunnerInExternal = 1;
							gunnerCompartments= "Compartment2";
							
							maxElev = 45;
							minElev = -20;
							maxTurn = 120;
							minTurn = -78;
							
							class TurnIn 
							{

								limitsArrayTop[] = 
								{

									{45, -120}, 
									{45, 120}
									
								};

								limitsArrayBottom[] = 
								{

									{-45, -120}, 
									{-45, 120}
									
								};

							};
							
							class TurnOut 
							{

								limitsArrayTop[] = 
								{

									{45, -78},
									{45, 120}
									
								};

								limitsArrayBottom[] = 
								{
									{-5, -78},
									{-20, 0},
									{-20, 47},
									{7, 57},
									{7, 100},
									{0, 110},
									{-5, 115},
									{-5, 120}
									
								};
							
							};
						
						};

						class CargoTurret_Rechts: CargoTurret_Links//[0,2] //Luke_rechts
						{
							
							gunnerInAction = "Redd_Marder_Passenger";
							gunnerName = "$STR_Dachluke_rechts";
							memoryPointsGetInGunner= "pos_hatch_2";
							memoryPointsGetInGunnerDir= "pos_hatch_2_dir";
							animationSourceHatch = "Hatch_right_Source";
							enabledByAnimationSource = "Hatch_right_rot";
							proxyIndex = 5;
							gunnerCompartments= "Compartment2";
							
							maxElev = 45;
							minElev = -20;
							maxTurn = -120;
							minTurn = 90;
							
							class TurnIn 
							{

								limitsArrayTop[] = 
								{

									{45, -120}, 
									{45, 120}
									
								};

								limitsArrayBottom[] = 
								{

									{-45, -120}, 
									{-45, 120}
									
								};

							};

							class TurnOut 
							{

								limitsArrayTop[] = 
								{

									{45, 78},
									{45, -120}
									
								};

								limitsArrayBottom[] = 
								{
									{-5, 88},
									{-18, 55},
									{-20, 0},
									{-20, -25},
									{-5, -30},
									{7, -35},
									{7, -70},
									{-5, -95},
									{-5, -120}
									
								};
							
							};
							
						};

						class CargoTurret_Hinten: CargoTurret_Links //[0,3] //Luke_hinten
						{
							
							gunnerInAction = "Redd_Marder_Passenger_Back";
							gunnerName = "$STR_Dachluke_hinten";
							memoryPointsGetInGunner= "pos_hatch_1";
							memoryPointsGetInGunnerDir= "pos_hatch_1_dir";
							animationSourceHatch = "Hatch_rear_Source";
							enabledByAnimationSource = "Hatch_rear_rot";
							proxyIndex = 6;
							gunnerCompartments= "Compartment2";
							
							maxElev = 45;
							minElev = -20;
							maxTurn = -80;
							minTurn = 100;

							class TurnIn 
							{

								limitsArrayTop[] = 
								{

									{45, -120}, 
									{45, 120}
									
								};

								limitsArrayBottom[] = 
								{

									{-45, -120}, 
									{-45, 120}
									
								};

							}; 

							class TurnOut 
							{

								limitsArrayTop[] = 
								{

									{45, 100},
									{45, -80}
									
								};

								limitsArrayBottom[] = 
								{
									{-7,100},
									{-7,70},
									{-20,60},
									{-20,0},
									{-20,-40},
									{-7,-50},
									{-7,-80}
									
								};
							
							};
						};

					};
					
					weapons[] =
					{
						
						"Redd_Gesichert",
						"Redd_MK20",
						"Redd_MG3"

					};

					magazines[] =
					{
						
						"Redd_MK20_HE_Mag",
						"Redd_MK20_AP_Mag",
						"Redd_Mg3_Mag",
						"Redd_Mg3_Mag",
						"Redd_Mg3_Mag",
						"Redd_Mg3_Mag",
						"Redd_Mg3_Mag",
						"Redd_Mg3_Mag",
						"Redd_Mg3_Mag"

					};
					
					class ViewOptics: ViewOptics
					{
						
						visionMode[] = {"Normal", "NVG", "TI"};
						thermalMode[] = {2,3};
						initFov = 0.4;
						minFov = 0.08;
						maxFov = 0.03;
						
					};
						
					class OpticsIn: Redd_Marder_Gunner_Optics
					{
						
						class Wide: Wide {};

						class Medium: Medium {};

						class Narrow: Narrow {};
					
					};
					
					startEngine = 0;
					stabilizedInAxes = 0;
					forceHideGunner = 1;
					initElev = 0;
					minElev = -13;
					maxElev = 65;
					initTurn = 0;
					minTurn = -360;
					maxTurn = 360;
					maxHorizontalRotSpeed = 4.5;// 1 = 45°/sec
					maxVerticalRotSpeed = 4.5;
					gunnerForceOptics=0;
					inGunnerMayFire=1;
					gunnerInAction="Redd_Marder_Gunner";
					gunnerAction="Redd_Marder_Gunner";
					proxyIndex = 2;
					viewGunnerInExternal = 1;
					hideProxyInCombat = 1;
					GunnerGetInAction="GetInLow";
					GunnerGetOutAction="GetOutLow";
					turretInfoType = "RscOptics_crows";
					canUseScanner = 0;
					allowTabLock = 0;
					ace_fcs_Enabled = 0;
					discreteDistance[] = {200,300,400,500,600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000};
					discreteDistanceInitIndex = 3;
					lodTurnedIn = 1100; //Pilot
					lodTurnedOut = 1100; //Pilot
					soundAttenuationTurret = "TankAttenuation";
					gunnerCompartments= "Compartment2";
					memoryPointsGetInGunner= "pos cargo";
					memoryPointsGetInGunnerDir= "dir cargo";

					soundServo[]=
					{
						
						"A3\Sounds_F\vehicles\armor\APC\noises\servo_APC_gunner",
						0.56234133,
						1,
						10
						
					};
					
					soundServoVertical[]=
					{
						
						"A3\Sounds_F\vehicles\armor\APC\noises\servo_APC_gunner_vertical",
						0.56234133,
						1,
						30
						
					};
					
					class HitPoints 
					{
						
						class HitTurret	
						{
							
							armor = 1;
							material = -1;
							name = "vez";
							visual="Camo1";
							passThrough = 0;
							minimalHit = 0.02;
							explosionShielding = 0.3;
							radius = 0.08;
							
						};
						
						class HitGun	
						{
							
							armor = 1;
							material = -1;
							name = "zbran";
							visual="Camo1";
							passThrough = 0;
							minimalHit = 0;
							explosionShielding = 1;
							radius = 0.10;
							
						};
					
					};
					
				};

				class Redd_Milan: NewTurret //[1]
				{
					
					weapons[] =
					{
						
						"Redd_Milan"
						
					};
					
					magazines[] = 
					{	
					
						"Redd_Milan_Mag",
						"Redd_Milan_Mag",
						"Redd_Milan_Mag",
						"Redd_Milan_Mag"
				
					};
					
					gunnerForceOptics=0;
					gunnerOutForceOptics = 0;
					gunnerCompartments= "Compartment4";
					proxyindex = 3;
					dontCreateAi = 1;
					body = "Milan_Rot_X";
					gun = "Milan_Rot_Y";
					missileBeg = "spice rakety";
					missileEnd = "konec rakety";
					animationSourceBody = "Milan_Rot_X";
					animationSourceGun = "Milan_Rot_Y";
					stabilizedInAxes = 3;
					gunnerAction  = "Redd_Marder_Commander_Milan"; 
					gunnerInAction="Redd_Marder_Commander_Milan";
					forceHideGunner = 1;
					viewGunnerInExternal = 1;
					gunnerDoor = "";
					memoryPointGunnerOptics = "Milan_View";
					gunnerOutOpticsModel = "";
					gunnerOpticsEffect[] = {};
					initElev = 0;
					minElev = -15;
					maxElev = 15;
					initTurn = 0;
					minTurn = -15;
					maxTurn = 15;
					canHideGunner = 1;
					startEngine = 0;
					isPersonTurret = 0;
					turretInfoType = "RscOptics_crows";
					canUseScanner = 0;
					allowTabLock = 0;
					ace_fcs_Enabled = 0;
					lodTurnedIn = 1000; //Gunner
					lodTurnedOut = 1000; //Gunner
					hideWeaponsGunner = 1;
					disableSoundAttenuation = 1;
					showAsCargo = 0;
					gunnerRightHandAnimName = "Milan_Turret";
					gunnerLeftHandAnimName = "Milan_Gun";

					class ViewOptics: ViewOptics
					{
						
						visionMode[] = {"Normal", "NVG", "TI"};
						thermalMode[] = {4,5};
						initFov = 0.4;
						minFov = 0.08;
						maxFov = 0.03;
						
					};
						
					class OpticsIn: Redd_Marder_Milan_Optics
					{
						
						class Wide: Wide{};
						
						class Medium: Medium{};
						
						class Narrow: Narrow{};
						
					};
					
				};
			
			};
			
			class AnimationSources
			{

				class HatchDriver
				{

					source = "user";
					initPhase = 0;
					animPeriod = 2;

				};

				class hatchCommander_Source
				{

					source = "user";
					initPhase = 0;
					animPeriod = 2;

				};

				class Hatch_left_Source
				{
				
					source = "user";
					animPeriod = 2;
					initPhase = 0;
				
				};

				class Hatch_right_Source
				{
				
					source = "user";
					animPeriod = 2;
					initPhase = 0;
				
				};

				class heck_luke_rotation
				{
				
					source = "user";
					animPeriod = 3;
					initPhase = 0;
				
				};
				class Hide_Knopf_Heck_luke
				{
				
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

				class rear_ramp_source
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 3;
					Sound = "Redd_Heckluke_sound";
					SoundPosition = "HecklukePoint";
					
				};
				
				class Hide_Milan_Source 
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};
				
				class Milan_Hide_Rohr 
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};
				
				class Spiegel_Source
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 2;
					
				};
				
				class recoil_source_1 
				{
					
					source = "reload"; 
					weapon = "Redd_MK20";
					
				};
				
				class recoil_source_2
				{
					
					source = "reload"; 
					weapon = "Redd_MG3";
					
				};

				class Redd_Sandsacke_Links
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};
				
				class Redd_Sandsacke_Rechts
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

				class TarnLichtHinten_Source
				{

					source = "user";
					initPhase = 1;
					animPeriod = 0;

				};

				class TarnLichtVorne_Source
				{

					source = "user";
					initPhase = 1;
					animPeriod = 0;

				};

				class LichterHide_Source
				{

					source = "user";
					initPhase = 0;
					animPeriod = 0;

				};

			};

			class UserActions
			{
				
				class heckluke_auf
				{
					
					displayName = "$STR_heckluke_auf";
					position = "HecklukePoint";
					radius = 10;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player in [driver this]) and (this animationPhase 'heck_luke_rotation' == 0) and (alive this)"; 
					statement = "this animate ['heck_luke_rotation', 1];this animate ['Hide_Knopf_Heck_luke', 0];";

				};
				
				class heckluke_zu
				{
					
					displayName = "$STR_heckluke_zu";
					position = "HecklukePoint";
					radius = 10;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player in [driver this]) and (this animationPhase 'heck_luke_rotation' > 0) and (alive this)"; 
					statement = "this animate['heck_luke_rotation', 0];this animate ['Hide_Knopf_Heck_luke', 1];";
				
				};

				class heckluke_auf_2: heckluke_auf
				{
					
					condition = "(this turretUnit [0,3] == player) and (this animationPhase 'heck_luke_rotation' == 0) and (alive this)"; 
					
				};
				
				class heckluke_zu_2: heckluke_zu
				{
					
					condition = "(this turretUnit [0,3] == player) and (this animationPhase 'heck_luke_rotation' > 0) and (alive this)"; 
					
				};
				
				class milan_auf
				{
					
					displayName = "$STR_milan_auf";
					position = "HecklukePoint";
					radius = 10;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player in [commander this]) and (this animationSourcePhase 'hatchCommander_Source' == 1) and (this animationSourcePhase 'Hide_Milan_Source' > 0) and (alive this)"; 
					statement = "this animateSource ['Hide_Milan_Source',0];this animate ['Milan_Hide_Rohr',0]";
					
				};

				class milan_ab
				{
					
					displayName = "$STR_milan_ab";
					position = "HecklukePoint";
					radius = 10;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player in [commander this]) and (this animationSourcePhase 'hatchCommander_Source' == 1) and (this animationSourcePhase 'Hide_Milan_Source' == 0) and (alive this)"; 
					statement = "this animateSource ['Hide_Milan_Source',1];this animate ['Milan_Hide_Rohr',1];";
					
				};

				class milan_in
				{
					
					displayName = "$STR_milan_in";
					position = "HecklukePoint";
					radius = 10;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player in [commander this]) and (this animationSourcePhase 'hatchCommander_Source' == 1) and (this animationSourcePhase 'Hide_Milan_Source' == 0) and (alive this)"; 
					statement = "(missionNamespace getVariable ['bis_fnc_moduleRemoteControl_unit', player]) action ['moveToTurret', this, [1]];[this,[[0,0],true]] remoteExecCall ['lockTurret'];"; 
					
				};

				class milan_aus
				{
					
					displayName = "$STR_milan_aus";
					position = "HecklukePoint";
					radius = 10;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this turretUnit [1] == player) and (alive this)"; 
					statement = "(missionNamespace getVariable ['bis_fnc_moduleRemoteControl_unit', player]) action ['moveToTurret', this, [0,0]];[this,[[0,0],false]] remoteExecCall ['lockTurret'];this setVariable ['Redd_Marder_Commander_Up', false];";
					
				};

				class WinkelsSpiegel_hoch
				{
					
					displayName = "$STR_Durch_Winkelspiegel_sehen";
					position = "HecklukePoint";
					radius = 10;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player in [commander this]) and (this animationSourcePhase 'hatchCommander_Source' == 0) and !(this getVariable 'Redd_Marder_Commander_Winkelspiegel') and (alive this)"; 
					statement = "[(missionNamespace getVariable ['bis_fnc_moduleRemoteControl_unit', player]),'Redd_Marder_Commander_Winkelspiegel'] remoteExecCall ['switchmove'];this setVariable ['Redd_Marder_Commander_Winkelspiegel', true];";
					
				};
				
				class WinkelsSpiegel_runter
				{
					
					displayName = "$STR_Wieder_hinsetzen";
					position = "HecklukePoint";
					radius = 10;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player in [commander this]) and (this animationSourcePhase 'hatchCommander_Source' == 0) and (this getVariable 'Redd_Marder_Commander_Winkelspiegel') and (alive this)"; 
					statement = "[(missionNamespace getVariable ['bis_fnc_moduleRemoteControl_unit', player]),'Redd_Marder_Commander'] remoteExecCall ['switchmove'];this setVariable ['Redd_Marder_Commander_Winkelspiegel', false];";
					
				};
				
				class Commander_hoch
				{
					
					displayName = "$STR_Hoeher_steigen";
					position = "HecklukePoint";
					radius = 10;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player in [commander this]) and (this animationSourcePhase 'hatchCommander_Source' == 1) and !(this getVariable 'Redd_Marder_Commander_Up') and (alive this)"; 
					statement = "[(missionNamespace getVariable ['bis_fnc_moduleRemoteControl_unit', player]),'Redd_Marder_Commander_Out_2'] remoteExecCall ['switchmove'];this setVariable ['Redd_Marder_Commander_Up', true];";
					
				};

				class Commander_tiefer
				{
					
					displayName = "$STR_Tiefer_steigen";
					position = "HecklukePoint";
					radius = 10;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player in [commander this]) and (this animationSourcePhase 'hatchCommander_Source' == 1) and (this getVariable 'Redd_Marder_Commander_Up') and (alive this)"; 
					statement = "[(missionNamespace getVariable ['bis_fnc_moduleRemoteControl_unit', player]),'Redd_Marder_Commander_Out_1'] remoteExecCall ['switchmove'];this setVariable ['Redd_Marder_Commander_Up', false];";
					
				};
				
				class Spiegel_einklappen
				{
					
					displayName = "$STR_Spiegel_einklappen";
					position = "HecklukePoint";
					radius = 10;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'Spiegel_Source' == 0) and (alive this)"; 
					statement = "this animateSource ['Spiegel_Source', 1];";
				
				};
				
				class Spiegel_ausklappen
				{
					
					displayName = "$STR_Spiegel_ausklappen";
					position = "HecklukePoint";
					radius = 10;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'Spiegel_Source' > 0) and (alive this)"; 
					statement = "this animateSource ['Spiegel_Source', 0];";
				
				};
				
				class TarnLichtHinten_ein
				{
					
					displayName = "$STR_Tarnbeleuchtung_hinten_ein";
					position = "HecklukePoint";
					radius = 10;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 1) and (this animationSourcePhase 'TarnLichtVorne_Source' == 1) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',1];this animateSource ['TarnLichtHinten_Source',0];";

				};
				
				class TarnLichtHinten_aus
				{
					
					displayName = "$STR_Tarnbeleuchtung_hinten_aus";
					position = "HecklukePoint";
					radius = 10;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 0) and (this animationSourcePhase 'TarnLichtVorne_Source' == 1) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',0];this animateSource ['TarnLichtHinten_Source',1];";

				};
				
				class TarnLichtVorne_ein
				{
					
					displayName = "$STR_Tarnbeleuchtung_vorne_ein";
					position = "HecklukePoint";
					radius = 10;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 1) and (this animationSourcePhase 'TarnLichtVorne_Source' == 1) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',1];this animateSource ['TarnLichtVorne_Source',0];";

				};
				
				class TarnLichtVorne_aus
				{
					
					displayName = "$STR_Tarnbeleuchtung_vorne_aus";
					position = "HecklukePoint";
					radius = 10;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 1) and (this animationSourcePhase 'TarnLichtVorne_Source' == 0) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',0];this animateSource ['TarnLichtVorne_Source',1];";

				};
				
				class Sandsacke_auf_Links
				{
					
					displayName = "$STR_Sandsaecke_aufbauen";
					position = "HecklukePoint";
					radius = 10;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this animationPhase 'Redd_Sandsacke_Links' > 0) and (this turretUnit [0,1] == player) and (this animationPhase 'Hatch_1_source' > 0) and (alive this)";
					statement = "this animate ['Redd_Sandsacke_Links',0]";
					
				};
				
				class Sandsacke_auf_Rechts
				{
					
					displayName = "$STR_Sandsaecke_aufbauen";
					position = "HecklukePoint";
					radius = 10;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this animationPhase 'Redd_Sandsacke_Rechts' > 0) and (this turretUnit [0,2] == player) and (this animationPhase 'Hatch_2_source' > 0) and (alive this)";
					statement = "this animate ['Redd_Sandsacke_Rechts',0]";
					
				};
				
				class Sandsacke_ab_Links
				{
					
					displayName = "$STR_Sandsaecke_abbauen";
					position = "HecklukePoint";
					radius = 10;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this animationPhase 'Redd_Sandsacke_Links' == 0) and (this turretUnit [0,1] == player) and (this animationPhase 'Hatch_1_source' > 0) and (alive this)";
					statement = "this animate ['Redd_Sandsacke_Links',1]";
					
				};
				
				class Sandsacke_ab_Rechts
				{
					
					displayName = "$STR_Sandsaecke_abbauen";
					position = "HecklukePoint";
					radius = 10;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this animationPhase 'Redd_Sandsacke_Rechts' == 0) and (this turretUnit [0,2] == player) and (this animationPhase 'Hatch_2_source' > 0) and (alive this)";
					statement = "this animate ['Redd_Sandsacke_Rechts',1]";
					
				};
				
			};
			
			class EventHandlers: EventHandlers 
			{
				
				init = "_this call redd_fnc_Marder_Init";
				fired = "_this call redd_fnc_Marder_Fired";
				getOut = "_this call redd_fnc_Marder_GetOut";
				turnIn = "_this call redd_fnc_Marder_TurnIn";
				turnOut = "_this call redd_fnc_Marder_TurnOut";
				reloaded = "_this call redd_fnc_Marder_Reload";
				
			};
			
			class Attributes 
			{
				
				class Redd_Marder_1A5_Bataillon_Attribute 
				{
					
					displayName = "$STR_Bataillon";
					tooltip = "$STR_Bataillon";
					property = "Redd_Marder_1A5_Bataillon_Attribute";
					control = "Combo";
					expression = "_this setVariable ['Redd_Marder_1A5_Bataillon', _value];";
					defaultValue = "0";
					typeName = "STRING";

					class values 
					{

						class Default
						{
							
							name = "-";
							value = "0";
							
						};
					
						class Bataillon_33 
						{
							
							name = "33";
							value = "1";
							
						};

						class Bataillon_92 
						{
							
							name = "92";
							value = "2";
							
						};

						class Bataillon_112  
						{
							
							name = "112";
							value = "3";
							
						};

						class Bataillon_122  
						{
							
							name = "122";
							value = "4";
							
						};
						
						class Bataillon_212  
						{
							
							name = "212";
							value = "5";
							
						};
						
						class Bataillon_371  
						{
							
							name = "371";
							value = "6";
							
						};
						
						class Bataillon_391  
						{
							
							name = "391";
							value = "7";
							
						};
						
						class Bataillon_401  
						{
							
							name = "401";
							value = "8";
							
						};
						
						class Bataillon_411  
						{
							
							name = "411";
							value = "9";
							
						};

						class Random
						{
							
							name = "$STR_Random";
							value = "10";
							
						};

					};
					
				};
				
				class Redd_Marder_1A5_Kompanie_Attribute 
				{
					
					displayName = "$STR_Kompanie";
					tooltip = "$STR_Kompanie";
					property = "Redd_Marder_1A5_Kompanie_Attribute";
					control = "Combo";
					expression = "_this setVariable ['Redd_Marder_1A5_Kompanie', _value];";
					defaultValue = "0";
					typeName = "STRING";

					class values 
					{

						class Default
						{
							
							name = "-";
							value = "0";
							
						};
					
						class Kompanie_1
						{
							
							name = "1";
							value = "1";
							
						};

						class Kompanie_2 
						{
							
							name = "2";
							value = "2";
							
						};

						class Kompanie_3  
						{
							
							name = "3";
							value = "3";
							
						};

						class Kompanie_4 
						{
							
							name = "4";
							value = "4";
							
						};

						class Random
						{
							
							name = "$STR_Random";
							value = "5";
							
						};
						
					};
					
				};
				
				class Redd_Marder_1A5_Zug_Buchstaben_Attribute 
				{
					
					displayName = "$STR_Zug_DisplayName";
					tooltip = "$STR_Zug_Buchstaben";
					property = "Redd_Marder_1A5_Zug_Buchstaben_Attribute";
					control = "Combo";
					expression = "_this setVariable ['Redd_Marder_1A5_Zug_Buchstabe', _value];";
					defaultValue = "0";
					typeName = "STRING";

					class values 
					{

						class Default
						{
							
							name = "-";
							value = "0";
							
						};
					
						class Alpha 
						{
							
							name = "Alpha";
							value = "1";
							
						};

						class Bravo 
						{
							
							name = "Bravo";
							value = "2";
							
						};

						class Charlie 
						{
							
							name = "Charlie";
							value = "3";
							
						};

						class Delta 
						{
							
							name = "Delta";
							value = "4";
							
						};

					};
		
				};
		
				class Redd_Marder_1A5_Fzg_Nummer_Attribute
				{
					
					displayName = "$STR_Fzg_Nummer_DisplayName";
					tooltip = "$STR_Fzg_Nummer";
					property = "Redd_Marder_1A5_Fzg_Nummer_Attribute";
					control = "Combo";
					expression = "_this setVariable ['Redd_Marder_1A5_Fzg_Nummer', _value];";
					defaultValue = "0";
					typeName = "STRING";

					class values 
					{

						class Default
						{
							
							name = "-";
							value = "0";
							
						};
					
						class Eins 
						{
							
							name = "1";
							value = "1";
							
						};

						class Zwei 
						{
							
							name = "2";
							value = "2";
							
						};

						class Drei 
						{
							
							name = "3";
							value = "3";
							
						};

						class Vier 
						{
							
							name = "4";
							value = "4";
							
						};

					};
					
				};
				
			};
			
		};
		
		/*	Faction variants	*/
		class Redd_Marder_1A5_Flecktarn: Redd_Marder_1A5_base 
		{
		
			//TODO: icon =
			side = 1;
			crew = "B_crew_F";
			typicalCargo[] = {"B_crew_F"};
			
			editorPreview="\Redd_Marder_1A5\pictures\Marder_F_Pre_Picture.paa";
			scope = 2;
            scopeCurator = 2;
			displayName="$STR_Marder_1A5";
			
			hiddenSelectionsTextures[] = 
			{

				"\Redd_Marder_1A5\data\Redd_Marder_1A5_Camo1_co.paa","\Redd_Marder_1A5\data\Redd_Marder_1A5_Camo2_co.paa"

			};
			
			class textureSources
			{
				class Fleck // Source class
				{
					displayName = "$STR_Redd_Flecktarn"; // name displayed, among other, from the garage
					author = "Tank, Pazuzu, Redd"; // Author of the skin
					textures[]=// List of textures, in the same order as the hiddenSelections definition
					{
						"\Redd_Marder_1A5\data\Redd_Marder_1A5_Camo1_co.paa",
						"\Redd_Marder_1A5\data\Redd_Marder_1A5_Camo2_co.paa"
					};
					factions[]=// This source should be available only for these factions
					{
						//could hold a list of factions what for the textures should only be available
					};
				};
				class Tropen
				{
					displayName = "$STR_Redd_Tropentarn"; // name displayed, among other, from the garage
					author = "Tank, Pazuzu, Redd"; // Author of the skin
					textures[]=// List of textures, in the same order as the hiddenSelections definition
					{
						"\Redd_Marder_1A5\data\Redd_Marder_1A5_Camo1_Trope_co.paa",
						"\Redd_Marder_1A5\data\Redd_Marder_1A5_Camo2_co.paa"
					};
					factions[]=// This source should be available only for these factions
					{
						//could hold a list of factions what for the textures should only be available
					};
				};
				class Winter
				{
					displayName = "$STR_Redd_Winter"; // name displayed, among other, from the garage
					author = "Tank, Pazuzu, Redd"; // Author of the skin
					textures[]=// List of textures, in the same order as the hiddenSelections definition
					{
						"\Redd_Marder_1A5\data\Redd_Marder_1A5_Camo1_Winter_co.paa",
						"\Redd_Marder_1A5\data\Redd_Marder_1A5_Camo2_co.paa"
					};
					factions[]=// This source should be available only for these factions
					{
						//could hold a list of factions what for the textures should only be available
					};
				};
			};
			textureList[]=
			{
				"Fleck", 1,
				"Tropen", 0,
				"Winter", 0
			};
			
		};
		
		/*	Public class Tropentarn	*/
		class Redd_Marder_1A5_Tropentarn: Redd_Marder_1A5_Flecktarn
		{
			
			editorPreview="\Redd_Marder_1A5\pictures\Marder_D_Pre_Picture.paa";
			scope=2;
            scopeCurator = 2;
			displayName="$STR_Marder_1A5_Tropentarn";
			hiddenSelectionsTextures[] = {"\Redd_Marder_1A5\data\Redd_Marder_1A5_Camo1_Trope_co.paa","\Redd_Marder_1A5\data\Redd_Marder_1A5_Camo2_co.paa"};
			
		};
		
		/*	Public class Wintertarn	*/
		class Redd_Marder_1A5_Wintertarn: Redd_Marder_1A5_Flecktarn 
		{
			
			editorPreview="\Redd_Marder_1A5\pictures\Marder_W_Pre_Picture.paa";
			scope=2;
            scopeCurator = 2;
			displayName="$STR_Marder_1A5_Winter";
			hiddenSelectionsTextures[] = {"\Redd_Marder_1A5\data\Redd_Marder_1A5_Camo1_Winter_co.paa","\Redd_Marder_1A5\data\Redd_Marder_1A5_Camo2_co.paa"};
			
		};
		
	};