

	//Triggerd by BI eventhandler "getout"
	
	_veh = _this select 0;

	waitUntil {!isNull _veh};
	
	_pos = _this select 1;
	_unit = _this select 2;
	_turret = _this select 3;
	
	//Driver
	if (_pos isEqualTo "driver") then
	{

		if (_veh animationPhase 'HatchDriver_source' > 0) then
		{
			waituntil {_veh animationPhase 'HatchDriver_source' == 1};
			_veh animateSource ['HatchDriver_source', 0];
			[_veh,true] remoteExecCall ['lockDriver'];

		};

	};

	//commander
	if (_turret isEqualTo [0,0]) then
	{

		if (_veh animationPhase 'hatchCommander_Source' > 0) then
		{
			waituntil {_veh animationPhase 'hatchCommander_Source' == 1};
			_veh animateSource ['hatchCommander_Source', 0];
			[_veh,[[0,0],true]] remoteExecCall ['lockTurret'];

		};

	};

	//gunner
	if (_turret isEqualTo [0]) then
	{
	
		[_veh,[[0],true]] remoteExecCall ['lockTurret'];

	};