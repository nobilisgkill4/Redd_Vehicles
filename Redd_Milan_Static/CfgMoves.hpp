

    class CfgMovesBasic
    {

        class DefaultDie;
        class ManActions
        {

            Redd_Milan_Gunner = "Redd_Milan_Gunner";
            
            KIA_Redd_Milan_Gunner = "KIA_Redd_Milan_Gunner";
            

        };

    };

    class CfgMovesMaleSdr: CfgMovesBasic
    {

        class States
        {

            class Crew;

            class Redd_Milan_Gunner: Crew
            {

                file = "\Redd_Milan_Static\anims\Redd_Milan_Gunner.rtm";
                interpolateTo[] = {"KIA_Redd_Milan_Gunner",1};
                ConnectTo[]={"KIA_Redd_Milan_Gunner", 1};

            };

            class KIA_Redd_Milan_Gunner: DefaultDie
		    {

                file = "\Redd_Milan_Static\anims\KIA_Redd_Milan_Gunner.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

        };

    };