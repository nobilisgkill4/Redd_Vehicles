

    class CfgPatches
	{
		
		class Redd_Milan_Static
		{
			
			units[] = {"B_Redd_Milan_Static","Redd_Milan_Static_Bag","Redd_Milan_Static_Tripod"};
			weapons[] = {};
			requiredVersion = 0.1;
			requiredAddons[] = {"A3_Static_F_Gamma","Redd_Vehicles_Main"};
			
		};
		
	};