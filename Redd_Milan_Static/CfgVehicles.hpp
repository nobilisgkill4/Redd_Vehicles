

    class CfgVehicles 
    {

        #include "cfgBags.hpp"

        class LandVehicle;

        class StaticWeapon: LandVehicle 
        {

            class Turrets;
            class MainTurret;
            class ViewOptics;
            class EventHandlers;

        };

        class StaticATWeapon: StaticWeapon {};

        class Redd_Milan_Static_Base: StaticATWeapon 
        {
            
            author = "Redd";
            editorCategory = "Redd_Vehicles";
            editorSubcategory = "Redd_Static";
			model = "\Redd_Milan_Static\Redd_Milan_Static";
            mapSize = 1.5;

            class Turrets: Turrets {

                class MainTurret: MainTurret 
                {

                    gunnerOpticsEffect[] = {};
                    initElev = 0;
                    minElev = -15;
                    maxElev = 15;
                    initTurn = 0;
                    minTurn = -30;
                    maxTurn = 30;
                    ejectDeadGunner = 0;
				    gunnerForceOptics = 0;
                    weapons[] = {"Redd_Milan"};
                    magazines[] = {"Redd_Milan_Mag", "Redd_Milan_Mag", "Redd_Milan_Mag", "Redd_Milan_Mag"};
                    gunnerAction  = "Redd_Milan_Gunner";
                    memoryPointsGetInGunner = "pos_gunner_dir";
                    memoryPointsGetInGunnerDir = "pos_gunner";
                    turretInfoType = "RscOptics_crows";
                    hideWeaponsGunner = 0;
                    getInAction="GetInLow";
			        getOutAction="GetInLow";

                    class ViewGunner 
                    {

                        initAngleX = 5;
                        minAngleX = -30;
                        maxAngleX = 30;
                        initAngleY = 0;
                        minAngleY = -100;
                        maxAngleY = 100;
                        initFov = 0.700000;
                        minFov = 0.250000;
                        maxFov = 1.100000;

				    };

                    class OpticsIn 
                    {

                        class Wide: ViewOptics
                        {

                            initAngleX = 0;
                            minAngleX = -30;
                            maxAngleX = 30;
                            initAngleY = 0;
                            minAngleY = -100;
                            maxAngleY = 100;
                            initFov = 0.4;
                            maxFov = 0.4;
                            minFov = 0.4;
                            visionMode[] = {"Normal", "NVG", "Ti"};
                            thermalMode[] = {4,5};
                            gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_Milan";
                            gunnerOpticsEffect[] = {};
                                        
                        };
                        
                        class Medium: Wide
                        {
                            
                            initFov = 0.08;
                            maxFov = 0.08;
                            minFov = 0.08;
                            visionMode[] = {"Normal", "NVG", "Ti"};
                            thermalMode[] = {4,5};
                            gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_Milan";
                            gunnerOpticsEffect[] = {};
                                        
                        };

                        class Narrow: Medium 
                        {
                            
                            initFov = 0.03;
                            maxFov = 0.03;
                            minFov = 0.03;
                            visionMode[] = {"Normal", "Ti", "NVG"};
                            thermalMode[] = {4,5};
                            gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_Milan";
                            gunnerOpticsEffect[] = {};
                                        
                        };

                    };

                    class HitPoints
				    {

                        class HitGun
                        {

                            armor = 0.3;
                            material = -1;
                            name = "Turret";
                            visual = "hitTurret";
                            passThrough = 0;
                            radius = 0.07;

                        };

					};

				};
   
            };

            class assembleInfo 
            {

                primary = 0;
                base = "";
                assembleTo = "";
                dissasembleTo[] = {"Redd_Milan_Static_Bag", "Redd_Milan_Static_Tripod"};
                displayName = "";

            };

            class AnimationSources 
            {
            
                class Milan_Hide_Rohr 
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};
            
            };

            class EventHandlers: EventHandlers 
            {
            
                fired = "_this call redd_fnc_Milan_Fired"; 
                reloaded = "_this spawn redd_fnc_Milan_Reload";
            
            };
            
            class Damage
		    {

		    	tex[]={};
			    mat[]=
                {

			    	"Redd_Vehicles_Main\mats\Redd_Milan.rvmat",
				    "Redd_Vehicles_Main\mats\Redd_Milan.rvmat",
				    "Redd_Vehicles_Main\mats\Redd_Milan_destruct.rvmat"

			    };

		    };
        
        };

        class B_Redd_Milan_Static: Redd_Milan_Static_Base 
        {
        
            side=1;
            scope=2;
            scopeCurator = 2;
            editorPreview="\Redd_Milan_Static\pictures\Milan_Pre_Picture.paa";	
            displayname = "$STR_Redd_Milan";
            crew = "B_Soldier_F";
            typicalCargo[] = {"B_Soldier_F"};

        };

    };
