

	class Bag_Base;

	class Weapon_Bag_Base: Bag_Base
	{

		class assembleInfo;

	};

	class Redd_Milan_Static_Bag: Weapon_Bag_Base
	{

		author="Redd";
		displayName = "$STR_Redd_Milan_Tube";
		mass=250;
		model = "Redd_Milan_Static\bags\Redd_Milan_Static_Bag";
		editorPreview="\Redd_Milan_Static\pictures\Milan_Bag_Pre_Picture.paa";
		editorCategory = "Redd_Vehicles";
        editorSubcategory = "Redd_Bags";
		side = 1;
		scope = 2;
		scopeCurator = 2;

		class assembleInfo: assembleInfo
		{

			displayName = "$STR_Redd_Milan";
			assembleTo="B_Redd_Milan_Static";

			base[]=
			{

				"Redd_Milan_Static_Tripod"

			};

		};

	};

	class Redd_Milan_Static_Tripod: Weapon_Bag_Base
	{

		mass=500;
		author="Redd";
		displayName = "$STR_Redd_Milan_Tripod";
		model = "Redd_Milan_Static\bags\Redd_Milan_Static_Tripod";
		editorPreview="\Redd_Milan_Static\pictures\Milan_Tripod_Pre_Picture.paa";
		editorCategory = "Redd_Vehicles";
        editorSubcategory = "Redd_Bags";
		side = 1;
		scope = 2;
		scopeCurator = 2;

		class assembleInfo
		{

			primary=0;
			base="";
			assembleTo="";
			dissasembleTo[]={};
			displayName="";

		};
		
	};
