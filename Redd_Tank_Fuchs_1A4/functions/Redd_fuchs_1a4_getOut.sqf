

	//Triggerd by BI eventhandler "getout"
	
	_veh = _this select 0;
	_pos = _this select 1;
	_unit = _this select 2;
	_turret = _this select 3;

	waitUntil {!isNull _veh};

	//check if unit is in mg3
	if (_turret isEqualTo [0]) then
	{	
		
		_veh setVariable ['Redd_Fuchs_MG3_In', false, true];
		[_veh,[[0,3],false]] remoteExecCall ['lockTurret'];

	};

	//check if unit is in milan
	if (_turret isEqualTo [1]) then
	{
		
		_veh setVariable ['Redd_Fuchs_Milan_In', false, true];
		[_veh,[[0,0],false]] remoteExecCall ['lockTurret'];

	};