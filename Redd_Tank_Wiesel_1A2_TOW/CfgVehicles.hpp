

	class Optics_Armored
	{
		
		class Wide;
		class Medium;
		class Narrow;
		
	};

	class Redd_Wiesel_TOW_Optics: Optics_Armored
	{
		
		class Wide: Wide
		{
			
			initFov = 0.4;
			maxFov = 0.4;
			minFov = 0.4;
			visionMode[] = {"Normal", "NVG", "Ti"};
			thermalMode[] = {4,5};
			gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_Milan";
			gunnerOpticsEffect[] = {};
						
		};
		
		class Medium: Medium
		{
			
			initFov = 0.08;
			maxFov = 0.08;
			minFov = 0.08;
			visionMode[] = {"Normal", "NVG", "Ti"};
			thermalMode[] = {4,5};
			gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_Milan";
			gunnerOpticsEffect[] = {};
						
		};
		class Narrow: Narrow 
		{
			
			initFov = 0.03;
			maxFov = 0.03;
			minFov = 0.03;
			visionMode[] = {"Normal", "NVG", "Ti"};
			thermalMode[] = {4,5};
			gunnerOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_Milan";
			gunnerOpticsEffect[] = {};
						
		};
		
	};

	class Redd_Wiesel_MG3_Optics: Optics_Armored
	{
		
		class Wide: Wide
		{
			
			initFov = 0.4;
			maxFov = 0.4;
			minFov = 0.4;
			visionMode[] = {"Normal", "NVG"};
			gunnerOpticsModel = "\A3\weapons_f\reticle\optics_empty";
			gunnerOpticsEffect[] = {};
						
		};
		
		class Narrow: Narrow 
		{
			
			initFov = 0.1;
			maxFov = 0.1;
			minFov = 0.1;
			visionMode[] = {"Normal", "NVG"};
			gunnerOpticsModel = "\A3\weapons_f\reticle\optics_empty";
			gunnerOpticsEffect[] = {};
						
		};
		
	};
	
	class CfgVehicles 
	{
		/*	A3 DEFAULT INHERITANCE TREE START */
		// Do not modify the inheritance tree, unless you want to alter game's internal configs, or REALLY know what you're doing.
		
		class LandVehicle;
		
		class Tank: LandVehicle 
		{
			
			class NewTurret;
			class Sounds;
			class HitPoints;
			
		};
		
		class Tank_F: Tank 
		{
			
			class Turrets 
			{
				
				class MainTurret:NewTurret 
				{
					
					class ViewOptics;
					class Turrets 
					{
						
						class CommanderOptics;
						
					};
					
				};
				
			};
			
			class EventHandlers;
			class AnimationSources;
			class ViewPilot;
			class ViewOptics;
			class ViewCargo;
			class HeadLimits;
			class CargoTurret;
			
			class HitPoints: HitPoints 
			{
				
				class HitHull;
				class HitFuel;
				class HitEngine;
				class HitLTrack;
				class HitRTrack;
				
			};
			
			class Sounds: Sounds 
			{
				
				class Engine;
				class Movement;
				
			};
			
		};
		
		/*	A3 DEFAULT INHERITANCE TREE END	*/
		
		/*	Base class	*/

		class APC_Tracked_01_base_F: Tank_F {};

		class Redd_Tank_Wiesel_1A2_TOW_base: APC_Tracked_01_base_F
		{
			
			#include "Sounds.hpp"
			#include "PhysX.hpp"
			#include "Pip.hpp"
			
			displayname = "$STR_Wiesel_1A2_TOW";
			author = "ReddnTank";
			model = "\Redd_Tank_Wiesel_1A2_TOW\Redd_Tank_Wiesel_1A2_TOW";	
			editorCategory = "Redd_Vehicles";
			editorSubcategory = "Redd_Waffentraeger";
			transportSoldier = 0;
			smokeLauncherGrenadeCount = 0;
			getInAction="GetInMedium";
			getOutAction="GetOutMedium";
			driverAction="Redd_Tank_Wiesel_1A2_TOW_Driver_Out";
			driverInAction="Redd_Tank_Wiesel_1A2_TOW_Driver";
			proxyIndex = 1;
			armor=150;
			armorStructural = 6;
			cost=1000000;
			driverForceOptics = 0;
			dustFrontLeftPos = "wheel_1_3_bound";
			dustFrontRightPos = "wheel_2_3_bound";
			dustBackLeftPos = "wheel_1_4_bound";
			dustBackRightPos = "wheel_2_4_bound";
			viewDriverInExternal = 1;
			lodTurnedIn = 1100; //Pilot
			lodTurnedOut = 1000; //Gunner
			enableManualFire = 0;
			insideSoundCoef = 0.9;
			attenuationEffectType = "TankAttenuation";
			driverOpticsModel = "\Redd_Vehicles_Main\data\Redd_optik_D";
			commanderCanSee = 31+32+14;
			threat[] = {1.0,0.6,0.6};
			driverCompartments = "Compartment1";
			aggregateReflectors[] = {{"Left","Right"}};
			selectionBrakeLights = "zadni svetlo";
			selectionBackLights = "brzdove svetlo";
			driverLeftHandAnimName 	= "drivewheel";
		    driverRightHandAnimName = "drivewheel";
			memoryPointSupply = "pos gunner";
			TFAR_AdditionalLR_Turret[] = {{1},{0,1}};
			forceHideDriver = 0;
			showNVGDriver = 0;
			showNVGCommander = 0;
			showNVGGunner = 0;
			memoryPointTrack1L = "Stopa LL";
			memoryPointTrack1R = "Stopa LR";
			memoryPointTrack2L = "Stopa RL";
			memoryPointTrack2R = "Stopa RR";
			slingLoadCargoMemoryPoints[] = {"SlingLoadCargo1","SlingLoadCargo2","SlingLoadCargo3","SlingLoadCargo4"};
			
			hiddenSelections[] = 
			{

				"wanne", //0
				"plate_1", //1
				"plate_2", //2
				"plate_3", //3
				"plate_4", //4
				"plate_5", //5
				"plate_6", //6

				"TakZeichen", //7
				"Bataillon", //8
				"Kompanie" //9
				
			};
			
			// Damage textures
			class Damage 
			{
				tex[] = {};
				mat[] = 
				{

					"Redd_Tank_Wiesel_1A2_TOW\mats\Redd_Tank_Wiesel_1A2_TOW_Wanne.rvmat",
					"Redd_Tank_Wiesel_1A2_TOW\mats\Redd_Tank_Wiesel_1A2_TOW_Wanne_damage.rvmat",
					"Redd_Tank_Wiesel_1A2_TOW\mats\Redd_Tank_Wiesel_1A2_TOW_Wanne_destruct.rvmat"

				};
				
        	};
			
			class ViewOptics: ViewOptics 
			{
				
				initFov = 1;
				maxFov = 1;
				minFov = 1;
				visionMode[] = {"Normal","NVG"};
				
			};

			class Exhausts 
			{
				class Exhaust1 
				{
					
					position = "exhaust";
					direction = "exhaust_dir";
					effect = "ExhaustsEffect";
					
				};
				
			};

			class Reflectors 
			{

				class Left 
				{
					
					color[] = {1900, 1800, 1700};
					ambient[] = {3, 3, 3};
                    position = "Light_L";
					direction = "Light_L_end";
					hitpoint = "Light_L";
					selection = "Light_L";
                    size = 1;
                    innerAngle = 75;
					outerAngle = 120;
					coneFadeCoef = 10;
                    intensity = 1;
                    useFlare = 1;
                    dayLight = 1;
                    flareSize = 1;

					class Attenuation 
					{
						
						start = 1;
						constant = 0;
						linear = 0;
						quadratic = 0.25;
						hardLimitStart = 30;
						hardLimitEnd = 60;
						
					};
					
				};
				
				class Right: Left 
				{
					
					position = "Light_R";
					direction = "Light_R_end";
					hitpoint = "Light_R";
					selection = "Light_R";
					
				};

			};

			class HitPoints: HitPoints 
			{
				
				class HitHull: HitHull 
				{	
				
					armor=2;
					material=-1;
					name="hull";
					visual="DamageVisual";
					passThrough=0;
					minimalHit = 0.14;
					explosionShielding = 2.0;
					radius = 0.15;
					
				};
				
				class HitFuel: HitFuel
				{
					
					armor = 0.5;
					material = -1;
					name = "fuel";
					visual="DamageVisual";
					passThrough = 0;
					minimalHit = 0.1;
					explosionShielding = 0.4;
					radius = 0.02;
					
				};
				
				class HitEngine: HitEngine 
				{
					
					armor=1;
					material=-1;
					name="engine";
					visual="DamageVisual";
					passThrough=0;
					minimalHit = 0.24;
					explosionShielding = 1;
					radius = 0.15;
					
				};
				
				class HitLTrack: HitLTrack 
				{
					
					armor=1;
					material=-1;
					name="pas_L";
					visual="DamageVisual";
					passThrough=0;
					minimalHit = 0.08;
					explosionShielding = 1.44;
					radius = 0.1;
					
				};
				
				class HitRTrack: HitRTrack 
				{
					
					armor=1;
					material=-1;
					name="pas_P";
					visual="DamageVisual";
					passThrough=0;
					minimalHit = 0.08;
					explosionShielding = 1.44;
					radius = 0.1;
					
				};

			};

			class TransportMagazines {};

			class TransportWeapons {};
			
			class TransportBackpacks {

				class _xx_B_AssaultPack_rgr
				{
					
					backpack = "B_AssaultPack_rgr";
					count = 1;
					
				};

			};
			
			class TransportItems 
			{

				class _xx_Toolkit 
				{
				
					name = "Toolkit";
					count = 1;
				
				};
			
			};

			class Turrets: Turrets 
			{
				
				class MainTurret: MainTurret //[0]
				{
					
					class Turrets: Turrets 
					{

						class CommanderOptics: CommanderOptics //[0,0] Commander
						{

							gunnerName = "$STR_Kommandant";
							turretCanSee = "CanSeeRadarC";
							gunnerForceOptics=0;
							gunnerOutForceOptics = 0;
							gunnerAction = "Redd_Tank_Wiesel_1A2_TOW_Commander_Out";
							gunnerInAction="Redd_Tank_Wiesel_1A2_TOW_Commander";
							gunnerOpticsEffect[] = {};
							gunnerOutOpticsModel = "";
							proxyIndex = 1;
							proxyType= "CPGunner";
							commanding = 1;
							LODTurnedIn = 1100; //Pilot 1100
							LODTurnedOut = 1000; //Gunner
							memoryPointGunnerOptics = "commanderview";
							gunnerOpticsModel = "\a3\weapons_f\reticle\Optics_Driver_01_F";
							weapons[] ={};
							memoryPointsGetInGunner= "pos commander";
							memoryPointsGetInGunnerDir= "pos commander dir";
							gunnerCompartments= "Compartment2";
							body = "";
							gun = "";
							animationSourceHatch = "Hatch_L_Rear_Source";

							class ViewOptics: ViewOptics 
							{
								
								initFov = 1;
								maxFov = 1;
								minFov = 1;
								visionMode[] = {"Normal","NVG"};
								
							};

						};

						class LoaderOptics: CommanderOptics //[0,1] Loader
						{
							
							gunnerName = "$STR_Ladeschuetze";
							turretCanSee = "CanSeeRadarC";
							gunnerForceOptics=0;
							gunnerOutForceOptics = 0;
							gunnerAction = "Redd_Tank_Wiesel_1A2_TOW_Loader_Out";
							gunnerInAction="Redd_Tank_Wiesel_1A2_TOW_Loader";
							gunnerOpticsEffect[] = {};
							gunnerOutOpticsModel = "";
							proxyIndex = 2;
							proxyType= "CPGunner";
							commanding = 0;
							LODTurnedIn = 1100; //Pilot 1100
							LODTurnedOut = 1000; //Gunner
							memoryPointGunnerOptics = "loaderview";
							gunnerOpticsModel = "\a3\weapons_f\reticle\Optics_Driver_01_F";
							weapons[] ={};
							memoryPointsGetInGunner= "pos loader";
							memoryPointsGetInGunnerDir= "pos loader dir";
							gunnerCompartments= "Compartment2";
							body = "";
							gun = "";
							animationSourceHatch = "Hatch_R_Rear_Source";

							class ViewOptics: ViewOptics 
							{
								
								initFov = 1;
								maxFov = 1;
								minFov = 1;
								visionMode[] = {"Normal","NVG"};
								
							};
							
						};

					};

					weapons[] =
					{
						
						"Redd_TOW"
						
					};
					
					magazines[] = 
					{	
					
						"Redd_TOW_Mag",
						"Redd_TOW_Mag",
						"Redd_TOW_Mag",
						"Redd_TOW_Mag"
				
					};

					gunnerName = "$STR_TOW";
					gunnerForceOptics=0;
					gunnerOutForceOptics = 0;
					gunnerCompartments= "Compartment4";
					proxyindex = 1;
					dontCreateAi = 1;
					cantCreateAI = 1;
					body = "TOW_Turret";
					gun = "TOW_Gun";
					missileBeg = "spice rakety";
					missileEnd = "konec rakety";
					animationSourceBody = "TOW_Turret_Source";
					animationSourceGun = "TOW_Gun_Source";
					stabilizedInAxes = 3;
					gunnerAction  = "Redd_Tank_Wiesel_1A2_TOW_Commander_TOW"; 
					gunnerInAction="Redd_Tank_Wiesel_1A2_TOW_Commander_TOW";
					forceHideGunner = 1;
					viewGunnerInExternal = 1;
					gunnerDoor = "";
					memoryPointGunnerOptics = "TOW_View";
					gunnerOutOpticsModel = "";
					gunnerOpticsEffect[] = {};
					initElev = 0;
					minElev = -15;
					maxElev = 15;
					initTurn = 0;
					minTurn = -15;
					maxTurn = 30;
					canHideGunner = 1;
					startEngine = 0;
					isPersonTurret = 0;
					turretInfoType = "RscOptics_crows";
					canUseScanner = 0;
					allowTabLock = 0;
					ace_fcs_Enabled = 0;
					lodTurnedIn = 1000; //Gunner
					lodTurnedOut = 1000; //Gunner
					hideWeaponsGunner = 1;
					disableSoundAttenuation = 1;
					showAsCargo = 0;
					gunnerRightHandAnimName = "TOW_Turret";
					gunnerLeftHandAnimName = "TOW_Turret";
					memoryPointsGetInGunner= "pos commander";
					memoryPointsGetInGunnerDir= "pos commander dir";

					class ViewOptics: ViewOptics
					{
						
						visionMode[] = {"Normal", "NVG", "TI"};
						thermalMode[] = {4,5};
						initFov = 0.4;
						minFov = 0.08;
						maxFov = 0.03;
						
					};
					
					class OpticsIn: Redd_Wiesel_TOW_Optics
					{
						
						class Wide: Wide{};
						
						class Medium: Medium{};
						
						class Narrow: Narrow{};
						
					};

					soundServo[]={};
					soundServoVertical[]={};
					
				};

				class Redd_MG3: NewTurret //[1]
				{
					
					weapons[] = {"Redd_MG3"};
                    magazines[] = 
                    {

                        "Redd_Mg3_Mag_250",
                        "Redd_Mg3_Mag_250",
                        "Redd_Mg3_Mag_250",
                        "Redd_Mg3_Mag_250",
                        "Redd_Mg3_Mag_250",
                        "Redd_Mg3_Mag_250",
                        "Redd_Mg3_Mag_250",
                        "Redd_Mg3_Mag_250",
                        "Redd_Mg3_Mag_250",
                        "Redd_Mg3_Mag_250"
                    
                    };
					
					gunnerName = "$STR_MG3";
					turretCanSee = "CanSeeRadarC";
					forceHideGunner = 1;
					dontCreateAi = 1;
					gunnerAction = "Redd_Tank_Wiesel_1A2_TOW_Loader_MG";
					gunnerInAction="Redd_Tank_Wiesel_1A2_TOW_Loader_MG";
					gunnerForceOptics=0;
					gunnerOutForceOptics = 0;
					gunnerOpticsEffect[] = {};
					gunnerOutOpticsModel = "";
					proxyIndex = 2;
					proxyType= "CPGunner";
					commanding = 0;
					startEngine = 0;
					stabilizedInAxes = 0;
					viewGunnerInExternal = 1;
					hideProxyInCombat = 1;
                    GunnerGetInAction="GetInMedium";
					GunnerGetOutAction="GetOutMedium";
                    primaryGunner = 0;
                    primaryObserver = 0;
					gunnerOpticsModel = "\A3\weapons_f\reticle\optics_empty";
					inGunnerMayFire=1;
					memoryPointGunnerOptics= "gunnerview2";
                    turretInfoType = "RscWeaponZeroing";
                    discreteDistance[] = {100,200,300,400,500,600,700,800,900,1000};
					discreteDistanceInitIndex = 4;
                    gunnerRightHandAnimName = "MG_Recoil";
				    gunnerLeftHandAnimName = "MG_Gun";
					hideWeaponsGunner = 1;
					isPersonTurret = 0;
					gunnerCompartments= "Compartment5";
					LODTurnedIn = 1000; //Gunner 1000
                    initElev = 0;
					minElev = -7;
					maxElev = 50;
					initTurn = 0;
					minTurn = -45;
					maxTurn = 38;
					disableSoundAttenuation = 1;
					body = "MG_Turret";
					gun = "MG_Gun";
					animationSourceBody = "MG_Turret_Source";
					animationSourceGun = "MG_Gun_Source";
					memoryPointsGetInGunner= "pos loader";
					memoryPointsGetInGunnerDir= "dir loader dir";
					
					class TurnIn
					{

						limitsArrayTop[]=
						{

							{50, -45},
							{50, 38}
							
						};

						limitsArrayBottom[]=
						{

							{-15, -45},
							{-15,-10},
							{-7, 0},
							{-7, 38}

						};
					
					};
					
                    class ViewOptics: ViewOptics 
			        {
				
				        initFov = 0.4;
			        	maxFov = 0.5;
				        minFov = 0.1;
				        visionMode[] = {"Normal","NVG"};
				
			        };

					class OpticsIn: Redd_Wiesel_MG3_Optics
					{

						class Wide: Wide{};
						
						class Narrow: Narrow{};

					};

                };

			};

			class AnimationSources
			{
				
				class HatchDriver
				{

					source = "user";
					initPhase = 0;
					animPeriod = 2;

				};

				class Hatch_L_Rear_Source
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 2;
					
				};

				class Hatch_R_Rear_Source
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 2;
					
				};
				
				class Seat_R_Trans
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 0.5;
					
				};

				class Seat_L_Trans
				{
					
					source = "user";
					initPhase = 0;
					animPeriod = 0.5;
					
				};

				class ReloadAnim
                {

                    source="reload";
                    weapon = "Redd_MG3";

                };

			    class ReloadMagazine
                {

                    source="reloadmagazine";
                    weapon = "Redd_MG3";
                    
                };

				class TarnLichtHinten_Source
				{
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

                class TarnLichtVorne_Source
                {
					
					source = "user";
					initPhase = 1;
					animPeriod = 0;
					
				};

				class LichterHide_Source
                {
					
					source = "user";
					initPhase = 0;
					animPeriod = 0;
					
				};
				class TOW_Hide_Rohr
				{

					source = "user";
					initPhase = 1;
					animPeriod = 0;

				};

				class Indicator_Source
				{

					source = "user";
					initPhase = 0;
					animPeriod = 1;

				};

			};

			class UserActions
			{
				
				class TOW_in
				{
					
					displayName = "$STR_TOW_in";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player in [commander this]) and (this animationSourcePhase 'Hatch_L_Rear_Source' == 1) and (alive this)";
					statement = "(missionNamespace getVariable ['bis_fnc_moduleRemoteControl_unit', player]) action ['moveToTurret', this, [0]];[this,[[0,0],true]] remoteExecCall ['lockTurret'];this animate ['Seat_L_Trans', 1];this animate ['TOW_Hide_Rohr', 0];";
					
				};

				class TOW_aus
				{
					
					displayName = "$STR_TOW_aus";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this turretUnit [0] == player) and (alive this)";
					statement = "(missionNamespace getVariable ['bis_fnc_moduleRemoteControl_unit', player]) action ['moveToTurret', this, [0,0]];[this,[[0,0],false]] remoteExecCall ['lockTurret'];this animate ['Seat_L_Trans', 0];this animate ['TOW_Hide_Rohr', 1];";
					
				};

				class MG3_in
				{
					
					displayName = "$STR_MG3_in";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this turretUnit [0,1] == player) and (this animationSourcePhase 'Hatch_R_Rear_Source' == 1) and (alive this)"; 
					statement = "(missionNamespace getVariable ['bis_fnc_moduleRemoteControl_unit', player]) action ['moveToTurret', this, [1]];[this,[[0,1],true]] remoteExecCall ['lockTurret'];this animate ['Seat_R_Trans', 1];";
     
				};

				class MG3_aus
				{
					
					displayName = "$STR_MG3_aus";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(this turretUnit [1] == player) and (alive this)";
					statement = "(missionNamespace getVariable ['bis_fnc_moduleRemoteControl_unit', player]) action ['moveToTurret', this, [0,1]];[this,[[0,1],false]] remoteExecCall ['lockTurret'];this animate ['Seat_R_Trans', 0];";
					
				};
				
                
                class TarnLichtHinten_ein
				{
					
					displayName = "$STR_Tarnbeleuchtung_hinten_ein";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 1) and (this animationSourcePhase 'TarnLichtVorne_Source' == 1) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',1];this animateSource ['TarnLichtHinten_Source',0];";

				};
				
				class TarnLichtHinten_aus
				{
					
					displayName = "$STR_Tarnbeleuchtung_hinten_aus";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 0) and (this animationSourcePhase 'TarnLichtVorne_Source' == 1) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',0];this animateSource ['TarnLichtHinten_Source',1];";

				};

				class TarnLichtVorne_ein
				{
					
					displayName = "$STR_Tarnbeleuchtung_vorne_ein";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 1) and (this animationSourcePhase 'TarnLichtVorne_Source' == 1) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',1];this animateSource ['TarnLichtVorne_Source',0];";

				};
				
				class TarnLichtVorne_aus
				{
					
					displayName = "$STR_Tarnbeleuchtung_vorne_aus";
					position = "actionPoint";
					radius = 25;
					onlyforplayer = 1;
					showWindow = 0;
					condition = "(player == driver this) and (this animationSourcePhase 'TarnLichtHinten_Source' == 1) and (this animationSourcePhase 'TarnLichtVorne_Source' == 0) and (alive this);";
					statement = "this animateSource ['LichterHide_Source',0];this animateSource ['TarnLichtVorne_Source',1];";

				};

				
			};

			class EventHandlers: EventHandlers 
			{

				init = "_this call redd_fnc_Wiesel_Init";
				getOut = "_this call redd_fnc_Wiesel_GetOut";
				engine = "_this call redd_fnc_Wiesel_engine";

			};

			class Attributes 
			{
				
				class Redd_Tank_Fuchs_1A4_Jg_Waffengattung_Attribute 
				{

					displayName = "$STR_Waffengattung";
					tooltip = "$STR_Waffengattung";
					property = "Redd_Tank_Wiesel_1A2_TOW_Waffengattung_Attribute";
					control = "Combo";
					expression = "_this setVariable ['Redd_Tank_Wiesel_1A2_TOW_Waffengattung', _value];";
					defaultValue = "0";
					typeName = "STRING";

					class values 
					{

						class Default
						{
							
							name = "-";
							value = "0";
							
						};
					
						class Jg
						{
							
							name = "Jaeger";
							value = "1";
							
						};

                        class GebJg 
						{
							
							name = "Gebirgsjaeger";
							value = "2";
							
						};

                        class Fallis 
						{
							
							name = "Fallschirmjaeger";
							value = "3";
							
						};

                    };

				};

				class Redd_Tank_Wiesel_1A2_TOW_Bataillon_Attribute 
				{

					displayName = "$STR_Bataillon";
					tooltip = "$STR_Bataillon";
					property = "Redd_Tank_Wiesel_1A2_TOW_Bataillon_Attribute";
					control = "Combo";
					expression = "_this setVariable ['Redd_Tank_Wiesel_1A2_TOW_Bataillon', _value];";
					defaultValue = "0";
					typeName = "STRING";

					class values 
					{

						class Default
						{
							
							name = "-";
							value = "0";
							
						};
					
						class Bataillon_1 
						{
							
							name = "JgBtl 1";
							value = "1";
							
						};

						class Bataillon_91
						{
							
							name = "JgBtl 91";
							value = "2";
							
						};

						class Bataillon_291 
						{
							
							name = "JgBtl 291";
							value = "3";
							
						};

						class Bataillon_292 
						{
							
							name = "JgBtl 292";
							value = "4";
							
						};
						
						class Bataillon_413 
						{
							
							name = "JgBtl 413";
							value = "5";
							
						};

                        class Bataillon_231
						{
							
							name = "GebJgBtl 231";
							value = "6";
							
						};

                        class Bataillon_232 
						{
							
							name = "GebJgBtl 232";
							value = "7";
							
						};

                        class Bataillon_233
						{
							
							name = "GebJgBtl 233";
							value = "8";
							
						};
						
						class Bataillon_26
						{
							
							name = "FJgReg 26";
							value = "9";
							
						};

						class Bataillon_31
						{
							
							name = "FJgReg 31";
							value = "10";
							
						};

					};

				};

				class Redd_Tank_Wiesel_1A2_TOW_Kompanie_Attribute 
				{
					
					displayName = "$STR_Kompanie";
					tooltip = "$STR_Kompanie";
					property = "Redd_Tank_Wiesel_1A2_TOW_Kompanie_Attribute";
					control = "Combo";
					expression = "_this setVariable ['Redd_Tank_Wiesel_1A2_TOW_Kompanie', _value];";
					defaultValue = "0";
					typeName = "STRING";

					class values 
					{

						class Default
						{
							
							name = "-";
							value = "0";
							
						};
				
						class Kompanie_5
						{
							
							name = "5";
							value = "1";
							
						};

						class Kompanie_7
						{
							
							name = "7";
							value = "2";
							
						};
						
					};
					
				};

			};

		};
			
		/*	Faction variants	*/
		class Redd_Tank_Wiesel_1A2_TOW_Flecktarn: Redd_Tank_Wiesel_1A2_TOW_base 
		{
		
			//TODO: icon =
			side = 1;
			crew = "B_crew_F";
			typicalCargo[] = {"B_crew_F"};
			
			editorPreview="";
			scope = 2;
            scopeCurator = 2;
			displayName="$STR_Wiesel_1A2_TOW";
			
			hiddenSelectionsTextures[] = 
			{

				"Redd_Tank_Wiesel_1A2_TOW\data\Redd_Tank_Wiesel_1A2_TOW_Wanne_blend_co.paa"

			};
			
			class textureSources
			{
				class Fleck // Source class
				{
					displayName = "$STR_Redd_Flecktarn"; // name displayed, among other, from the garage
					author = "Tank, Pazuzu, Redd"; // Author of the skin
					textures[]=// List of textures, in the same order as the hiddenSelections definition
					{
						"Redd_Tank_Wiesel_1A2_TOW\data\Redd_Tank_Wiesel_1A2_TOW_Wanne_blend_co.paa"
					};
					factions[]=// This source should be available only for these factions
					{
						//could hold a list of factions what for the textures should only be available
					};
				};
				class Tropen
				{
					displayName = "$STR_Redd_Tropentarn"; // name displayed, among other, from the garage
					author = "Tank, Pazuzu, Redd"; // Author of the skin
					textures[]=// List of textures, in the same order as the hiddenSelections definition
					{
						"Redd_Tank_Wiesel_1A2_TOW\data\Redd_Tank_Wiesel_1A2_TOW_Wanne_D_blend_co.paa"
					};
					factions[]=// This source should be available only for these factions
					{
						//could hold a list of factions what for the textures should only be available
					};
				};
				class Winter
				{
					displayName = "$STR_Redd_Winter"; // name displayed, among other, from the garage
					author = "Tank, Pazuzu, Redd"; // Author of the skin
					textures[]=// List of textures, in the same order as the hiddenSelections definition
					{
						"Redd_Tank_Wiesel_1A2_TOW\data\Redd_Tank_Wiesel_1A2_TOW_Wanne_W_blend_co.paa"
					};
					factions[]=// This source should be available only for these factions
					{
						//could hold a list of factions what for the textures should only be available
					};
				};
			};
			textureList[]=
			{
				"Fleck", 1,
				"Tropen", 0,
				"Winter", 0
			};
			
		};
		
		/*	Public class Tropentarn	*/
		class Redd_Tank_Wiesel_1A2_TOW_Tropentarn: Redd_Tank_Wiesel_1A2_TOW_Flecktarn
		{
			
			editorPreview="";
			scope=2;
            scopeCurator = 2;
			displayName="$STR_Wiesel_1A2_TOW_Tropentarn";

			hiddenSelectionsTextures[] = 
			{

				"Redd_Tank_Wiesel_1A2_TOW\data\Redd_Tank_Wiesel_1A2_TOW_Wanne_D_blend_co.paa"

			};

			
		};
		
		/*	Public class Wintertarn	*/
		class Redd_Tank_Wiesel_1A2_TOW_Wintertarn: Redd_Tank_Wiesel_1A2_TOW_Flecktarn 
		{
			
			editorPreview="";
			scope=2;
            scopeCurator = 2;
			displayName="$STR_Wiesel_1A2_TOW_Winter";

			hiddenSelectionsTextures[] = 
			{

				"Redd_Tank_Wiesel_1A2_TOW\data\Redd_Tank_Wiesel_1A2_TOW_Wanne_W_blend_co.paa"

			};

			
		};
		
	};