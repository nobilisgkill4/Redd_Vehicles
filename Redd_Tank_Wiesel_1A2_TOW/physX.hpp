	
	
	//Basic parameters
	simulation = TankX;
	dampersBumpCoef = 0.3;
	terrainCoef = 0;

	//Fuel
	#define FUEL_FACTOR 0.165
	fuelCapacity = 200 * FUEL_FACTOR;
    ACE_refuel_fuelCapacity = 200;

	//Differential parameters
	//Nix

	//Engine parameters
	maxOmega = 628.32;
	minOmega = 104.72;
	enginePower = 65;
	peakTorque = 220;
	engineMOI = 1;
	idleRpm = 1000;
	redRpm = 6000;
	clutchStrength = 14;
	dampingRateFullThrottle = 0.25;
	dampingRateZeroThrottleClutchEngaged = 0.5;
	dampingRateZeroThrottleClutchDisengaged = 0.25;
	maxSpeed = 75;
	thrustDelay	= 0.05;
	brakeIdleSpeed = 2.8;
	normalSpeedForwardCoef = 0.43;
	slowSpeedForwardCoef = 0.15;
	
	tankTurnForce = 11*2800;
	tankTurnForceAngMinSpd = 1;
	tankTurnForceAngSpd = 1;
	
	accelAidForceCoef = 4;
	accelAidForceYOffset = -1;
	accelAidForceSpd = 13;

	torqueCurve[] = 
	{

		{"(0/6000)","(0/220)"},
		{"(857/6000)","(220/220)"},
		{"(1714/6000)","(220/220)"},
		{"(2571/6000)","(220/220)"},
		{"(3428/6000)","(178/220)"},
		{"(4285/6000)","(142/220)"},
		{"(5142/6000)","(118/220)"},
		{"(6000/6000)","(0/220)"}

	};

	//Floating and sinking
	waterPPInVehicle=0;
	maxFordingDepth=-0.75;
	waterResistance=0;
	canFloat = 0;
	waterLeakiness = 10;
	
	//Anti-roll bars
	antiRollbarForceCoef = 85;
	antiRollbarForceLimit = 85;
	antiRollbarSpeedMin = 0;
	antiRollbarSpeedMax	= 55;

	//Gearbox
	class complexGearbox 
	{
	
		//GearboxRatios[] = {"R1", -6.5, "N", 0, "D1", 4, "D2", 3.2, "D3", 2.6}; //75KM/H
		GearboxRatios[] = {"R1", -6.5, "N", 0, "D1", 4, "D2", 3.5, "D3", 3.75};
		TransmissionRatios[] = {"High",6};
		gearBoxMode        = "auto";
		moveOffGear        = 1;
		driveString        = "D";
		neutralString      = "N";
		reverseString      = "R";
		transmissionDelay  = 0.1;

	};

	changeGearType="rpmratio";
	
	changeGearOmegaRatios[]=
	{

		1.0,0.6,
		0.9,0.6,
		0.6,0.2,
		0.8,0.4,
		1,0.6
	};

	switchTime = 0;
	latency = 0.5;
	engineLosses = 25;
	transmissionLosses = 15;

	//Wheel parameters
	wheelCircumference= 1.51;
	numberPhysicalWheels = 8;
	turnCoef = 5;

	class Wheels
	{
		class L2
		{
			
			boneName = "wheel_podkoloL1";
			center   = "wheel_1_2_axis";
			boundary = "wheel_1_2_bound";
			damping = 75;
			steering = 0;
			side = "left";
			mass = 150;
			weight = 150;
			width = 0.07;
			MOI = 5.5;
			latStiffX = 25;
			latStiffY = 280;
			longitudinalStiffnessPerUnitGravity = 1500;
			maxBrakeTorque = 2800/8;
			sprungMass = 2800/8;
			springStrength = 35000;
			springDamperRate = 9100;
			dampingRate = 1;
			dampingRateInAir = 410;
			dampingRateDamaged = 10;
			dampingRateDestroyed = 10000;
			maxCompression = 0.15;
			maxDroop = 0.15;
			frictionVsSlipGraph[] = {{ 0,0.75 },{ 0.5,1.5 },{ 1,3}};
			
		};
		
		class L3: L2
		{
			
			boneName="wheel_podkolol2";
			center="wheel_1_3_axis";
			boundary="wheel_1_3_bound";
			
		};
		
		class L4: L2
		{
			
			boneName="wheel_podkolol3";
			center="wheel_1_4_axis";
			boundary="wheel_1_4_bound";
			
		};
		
		class L5: L2
		{
			
			boneName="wheel_podkolol4";
			center="wheel_1_5_axis";
			boundary="wheel_1_5_bound";
			
		};

		//Triebrad
		class L1: L2
		{
			
			boneName="";
			center="wheel_1_1_axis";
			boundary="wheel_1_1_bound";
			maxDroop=0;
			maxCompression=0;

		};

		class R2: L2
		{
			
			side="right";
			boneName="wheel_podkolop1";
			center="wheel_2_2_axis";
			boundary="wheel_2_2_bound";
			
		};
		
		class R3: R2
		{
			
			boneName="wheel_podkolop2";
			center="wheel_2_3_axis";
			boundary="wheel_2_3_bound";
			
		};

		class R4: R2
		{
			
			boneName="wheel_podkolop3";
			center="wheel_2_4_axis";
			boundary="wheel_2_4_bound";
			
		};
		
		class R5: R2
		{
			
			boneName="wheel_podkolop4";
			center="wheel_2_5_axis";
			boundary="wheel_2_5_bound";
			
		};
		
		//Triebrad
		class R1: R2
		{
			
			boneName="";
			center="wheel_2_1_axis";
			boundary="wheel_2_1_bound";
			maxDroop=0;
			maxCompression=0;
			
		};

	};