	
	
	class RenderTargets
	{
		
		//Driver center
		class Mirror1
		{

			renderTarget = "rendertarget0";
			
			class CameraView1
			{

				pointPosition		= "d_center_pos";
				pointDirection		= "d_center_dir";
				renderQuality 		= 1;
				renderVisionMode 	= 0;
				fov 				= 0.7;	
						
			}; 	

		};
		
		//Driver left 
		class Mirror2
		{

			renderTarget = "rendertarget1";

			class CameraView1
			{

				pointPosition		= "d_left_pos";
				pointDirection		= "d_left_dir";
				renderQuality 		= 1;
				renderVisionMode 	= 0;
				fov 				= 0.7;

			}; 

		};

		//Driver Right
		class Mirror3
		{

			renderTarget = "rendertarget2";

			class CameraView1
			{

				pointPosition		= "d_right_pos";
				pointDirection		= "d_right_dir";
				renderQuality 		= 1;
				renderVisionMode 	= 0;
				fov 				= 0.7;

			}; 

		};

		//Commander center
		class Mirror4
		{

			renderTarget = "rendertarget3"; 
			
			class CameraView1
			{

				pointPosition		= "c_center_pos";
				pointDirection		= "c_center_dir";
				renderQuality 		= 1;			
				renderVisionMode 	= 0;			
				fov 				= 0.7;	
						
			}; 	

		};

		//Commander side
		class Mirror5
		{

			renderTarget = "rendertarget4";

			class CameraView1
			{

				pointPosition		= "c_side_pos";
				pointDirection		= "c_side_dir";
				renderQuality 		= 1;
				renderVisionMode 	= 0;
				fov 				= 0.7;	

			}; 

		};

		//Loader center
		class Mirror6
		{

			renderTarget = "rendertarget5";

			class CameraView1
			{

				pointPosition		= "l_center_pos";
				pointDirection		= "l_center_dir";
				renderQuality 		= 1;
				renderVisionMode 	= 0;
				fov 				= 0.7;

			}; 	

		};

		//Loader side
		class Mirror7
		{

			renderTarget = "rendertarget6";

			class CameraView1
			{

				pointPosition		= "l_side_pos";
				pointDirection		= "l_side_dir";
				renderQuality 		= 1;
				renderVisionMode 	= 0;
				fov 				= 0.7;

			}; 	

		};
		
	};
