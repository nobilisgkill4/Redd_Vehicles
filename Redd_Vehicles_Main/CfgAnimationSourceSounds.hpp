

	class CfgAnimationSourceSounds
	{

		class Redd_Heckluke_sound
		{
			
			class DoorMovement 
			{
				
				loop = 0;
				terminate = 1;
				trigger = "(phase factor[0.05,0.10]) * (phase factor[0.95,0.9])";
				sound0[] = {"\Redd_Vehicles_Main\sounds\Heckluke_sound.ogg", 1, 1, 60};
				sound[] = {"sound0", 1};
				
			};
			
			class DoorMovementDone1 
			{
				
				loop = 0;
				terminate = 0;
				trigger = "direction * (phase factor[0.9,0.99])";
				sound0[] = {"A3\Sounds_F\sfx\doors\ServoDoors\ServoDoorsSlam", 1, 1, 60};
				sound[] = {"sound0", 1};
				
			};

			class DoorMovementDone2 
			{
				
				loop = 0;
				terminate = 0;
				trigger = "(1 - direction) * (phase factor[0.1,0.05])";
				sound0[] = {"A3\Sounds_F\sfx\doors\ServoDoors\ServoDoorsSlam", 1, 1, 60};
				sound[] = {"sound0", 1};
				
			};

			class SlamTheDoor 
			{
				
				loop = 0;
				terminate = 0;
				trigger = "(1 - direction) * (phase factor[0.02,0.01])";
				sound0[] = {"A3\Sounds_F\sfx\doors\ServoDoors\ServoDoorsSlam", 1, 1, 60};
				sound[] = {"sound0", 1};
				
			};
			
		};
		
	};