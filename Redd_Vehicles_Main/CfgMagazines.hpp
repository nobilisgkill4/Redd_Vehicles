	
	
	class CfgAmmo
	{
		
		class B_30mm_HE_Tracer_Red;
		class B_30mm_APFSDS_Tracer_Red;
		class SmokeLauncherAmmo;
		class FlareCore;
		class M_Titan_AT;
		
		class Redd_B_30mm_HE_Tracer_Red: B_30mm_HE_Tracer_Red
		{
			
			typicalSpeed = 1050;
			irLock = 0;
			airLock = 0;
			laserLock = 0;
			nvlock = 0;
			canLock = 0;
			tracerScale = 3;
			tracerStartTime = 0.1;
			tracerEndTime = 5.7;
			hit = 25;
			indirectHit = 15;
			indirectHitRange = 5;
			caliber = 2.5;
			explosive = 0.95;

			class CamShakeFire
			{
				
				power = 1;
				duration = 1;
				frequency = 20;
				distance = 10;
				
			};
			
		};
		
		class Redd_B_30mm_APFSDS_Tracer_Red: B_30mm_APFSDS_Tracer_Red
		{
			
			typicalSpeed = 1050;
			irLock = 0;
			airLock = 0;
			laserLock = 0;
			nvlock = 0;
			canLock = 0;
			tracerScale = 3;
			tracerStartTime = 0.1;
			tracerEndTime = 5.7;
			hit = 100;
			caliber = 5.2;
			class CamShakeFire
			{
				
				
				power = 1;
				duration = 1;
				frequency = 20;
				distance = 10;
				
			};
			
		};
		
		class Redd_SmokeLauncherAmmo: SmokeLauncherAmmo 
		{
			
			muzzleEffect = "";
			effectsSmoke = "EmptyEffect";
			weaponLockSystem = "1 + 2 + 4";
			hit = 1;
			indirectHit = 0;
			indirectHitRange = 0;
			timeToLive = 10.0;
			thrustTime = 10.0;
			airFriction = -0.1;
			simulation = "shotCM";
			model = "\A3\weapons_f\empty";
			maxControlRange = 50;
			initTime = 2;
			aiAmmoUsageFlags = "4 + 8";
			
		};
		
		class Redd_SmokeShellSubVehicle: FlareCore 
		{
		
			model = "\A3\weapons_f\ammo\flare_white";
			deflecting = 10;
			lightColor[] = {1, 0.500000, 0, 0.200000};
			useFlare = 1;
			flareSize = 1;
			smokeColor[] = {1, 1, 1, 0.500000};
			muzzleEffect = "BIS_fnc_effectFiredRifle";
			effectFlare = "CounterMeasureFlare";
			brightness = 0.500000;
			size = 0.200000;
			triggerTime = 0;
			triggerSpeedCoef = 1;
			
		};
		
		class Redd_Milan_AT: M_Titan_AT
		{
			
			canLock = 0;
			irLock = 0;
			airLock = 0;
			laserLock = 0;
			nvlock = 0;
			manualControl = 1;
			maxControlRange = 3000;
			maxSpeed = 270;
			thrustTime = 2;
			thrust=45;
			fuseDistance = 65;
			
		};

		class Redd_TOW_AT: Redd_Milan_AT
		{

			maxControlRange = 4000;

		};
		
	};
	
	class CfgMagazines 
	{
		
		class 140Rnd_30mm_MP_shells_Tracer_Red;
		class 60Rnd_30mm_APFSDS_shells_Tracer_Red;
		class 1000Rnd_762x51_Belt_Red;
		class 2Rnd_GAT_missiles;
		class SmokeLauncherMag;
		
		class Redd_MK20_HE_Mag: 140Rnd_30mm_MP_shells_Tracer_Red
		{
			
			scope = 2;
			displayName = "20mm HEI";
			displayNameShort = "HEI";
			ammo = "Redd_B_30mm_HE_Tracer_Red";
			count = 700;
			tracersEvery = 1;
			
		};
		
		class Redd_MK20_AP_Mag: 60Rnd_30mm_APFSDS_shells_Tracer_Red
		{
			
			scope = 2;
			displayName = "20mm APDS";
			displayNameShort = "APDS";
			ammo = "Redd_B_30mm_APFSDS_Tracer_Red";
			count = 350;
			tracersEvery = 1;
			
		};
		
		class Redd_Mg3_Mag: 1000Rnd_762x51_Belt_Red
		{
			
			scope = 2;
			displayName = "$STR_Redd_MG3_500-Schuss-Gurtkasten";
			displayNameShort = "";
			ammo = "B_762x51_Tracer_Red";
			count = 500;
			tracersEvery = 3;
			
		};

		class Redd_Mg3_Mag_250: Redd_Mg3_Mag
		{
			
			count = 250;
			
		};
		
		class Redd_Milan_Mag: 2Rnd_GAT_missiles
		{
			
			scope = 2;
			displayName = "Milan";
			displayNameShort = "";
			ammo = "Redd_Milan_AT";
			count = 1;
			initSpeed = 180;
			maxLeadSpeed = 60;
			
		};

		class Redd_TOW_Mag: Redd_Milan_Mag
		{
			
			displayName = "TOW";
			ammo = "Redd_TOW_AT";
			
		};
		
		class Redd_SmokeLauncherMag: SmokeLauncherMag 
		{
			
			ammo = "Redd_SmokeLauncherAmmo";
		
		};
	
	};