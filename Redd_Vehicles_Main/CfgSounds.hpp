

	class CfgSounds 
	{

		class Redd_SmokeLauncher_Fire 
		{
			
			sound[] = {"\Redd_Vehicles_Main\sounds\bwa3_smokelauncher_fire", 3, 1, 50};
			titles[] = {};
			
		};

		class Redd_SmokeLauncher_Explosion 
		{
			
			sound[] = {"\Redd_Vehicles_Main\sounds\bwa3_smokelauncher_explosion", 3, 1, 50};
			titles[] = {};
			
		};
		
	};