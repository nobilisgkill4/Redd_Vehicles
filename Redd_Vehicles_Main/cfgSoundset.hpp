
	
	class CfgSoundSets 
	{
		
		class Redd_MG3_Shot_SoundSet
		{
			
			soundShaders[] = {"Redd_MG3_Closure_SoundShader","Redd_MG3_closeShot_SoundShader","Redd_MG3_midShot_SoundShader", "Redd_MG3_distShot_SoundShader"};
			volumeFactor=1.4;
			volumeCurve="InverseSquare2Curve";
			spatial=1;
			doppler=0;
			loop=0;
			sound3DProcessingType="WeaponMediumShot3DProcessingType";
			distanceFilter="weaponShotDistanceFreqAttenuationFilter";
			occlusionFactor=0.5;
			obstructionFactor=0.3;
			
		};
		
		class Redd_MG3_Tail_SoundSet
		{
		
			soundShaders[] = {"Redd_MG3_tailDistant_SoundShader", "Redd_MG3_tailTrees_SoundShader", "Redd_MG3_tailForest_SoundShader", "Redd_MG3_tailMeadows_SoundShader", "Redd_MG3_tailHouses_SoundShader"};
			volumeFactor=0.7;
			volumeCurve="InverseSquare2Curve";
			spatial=1;
			doppler=0;
			loop=0;
			soundShadersLimit=3;
			sound3DProcessingType="WeaponMediumShotTail3DProcessingType";
			distanceFilter="weaponShotTailDistanceFreqAttenuationFilter";
			occlusionFactor=0.3;
			obstructionFactor=0;
		
		};
		
		class Redd_Mk20_Shot_SoundSet
		{
			
			soundShaders[] = {"Redd_MK20_closeShot_SoundShader", "Redd_MK20_midShot_SoundShader", "Redd_MK20_distShot_SoundShader"};
			volumeFactor = 1.6;
			frequencyFactor = 1.1;
			volumeCurve="InverseSquare2Curve";
			spatial=1;
			doppler=0;
			loop=0;
			sound3DProcessingType="WeaponMediumShot3DProcessingType";
			distanceFilter="weaponShotDistanceFreqAttenuationFilter";
			occlusionFactor=0.5;
			obstructionFactor=0.3;
	
		};
		
		class Redd_MK20_Tail_SoundSet
		{
		
			soundShaders[] = {"Redd_MK20_tailDistant_SoundShader","Redd_MK20_tailForest_SoundShader","Redd_MK20_tailHouses_SoundShader","Redd_MK20_tailMeadows_SoundShader","Redd_MK20_tailTrees_SoundShader"};
			volumeFactor = 1.7;
			volumeCurve="InverseSquare2Curve";
			spatial=1;
			doppler=0;
			loop=0;
			soundShadersLimit=3;
			sound3DProcessingType="WeaponMediumShotTail3DProcessingType";
			distanceFilter="weaponShotTailDistanceFreqAttenuationFilter";
			occlusionFactor=0.3;
			obstructionFactor=0;

		};

		class Redd_Milan_Shot_SoundSet
		{

			soundShaders[] = {"Redd_Milan_closeShot_SoundShader", "Redd_Milan_midShot_SoundShader", "Redd_Milan_distShot_SoundShader"};
			volumeFactor = 1.5;
			volumeCurve="LinearCurve";
			spatial=1;
			doppler=0;
			loop=0;
			sound3DProcessingType="ExplosionMedium3DProcessingType";
			distanceFilter="explosionDistanceFreqAttenuationFilter";

		};

		class Redd_Milan_Tail_SoundSet
		{

			soundShaders[] = {"Redd_Milan_tailForest_SoundShader", "Redd_Milan_tailMeadows_SoundShader", "Redd_Milan_tailHouses_SoundShader"};
			volumeFactor = 1;
			volumeCurve="InverseSquare2Curve";
			spatial=1;
			doppler=0;
			loop=0;
			soundShadersLimit=2;
			frequencyRandomizer=0.05;
			sound3DProcessingType="ExplosionMediumTail3DProcessingType";
			distanceFilter="explosionTailDistanceFreqAttenuationFilter";

		};
		
	};