	
	
	#include "CfgPatches.hpp"
	#include "CfgEditorClass.hpp"
	#include "CfgCloudlets.hpp"
	#include "CfgMagazines.hpp"
	#include "CfgWeapons.hpp"
	#include "CfgSounds.hpp"
	#include "CfgSoundset.hpp"
	#include "CfgSoundshaders.hpp"
	#include "CfgAnimationSourceSounds.hpp"
	#include "CfgFunctions.hpp"
	
	class CfgVehicles 
	{

		class thingX;

		class Redd_Milan_Rohr: thingX 
		{
		
			scope = 1;
			displayName = "$STR_Milan_Rohr_Leer";
			model = "\Redd_Vehicles_Main\data\Redd_Milan_Rohr.p3d";
			
		};

		class Redd_Wiesel_Wreck: thingX 
		{
		
			scope = 1;
			displayName = "Wiesel_Wreck";
			model = "\Redd_Tank_Wiesel_1A2_TOW\Redd_Tank_W_1A2_TOW_Wreck.p3d";
			
		};

		class Redd_Fuchs_Wreck: thingX 
		{
		
			scope = 1;
			displayName = "Fuchs_Wreck";
			model = "\Redd_Tank_Fuchs_1A4\Redd_Tank_Fuchs_1A4_Wreck.p3d";
			
		};

		class Redd_Marder_Wreck: thingX 
		{
		
			scope = 1;
			displayName = "Fuchs_Wreck";
			model = "\Redd_Marder_1A5\Redd_Marder_1A5_Wreck.p3d";
			
		};

	};