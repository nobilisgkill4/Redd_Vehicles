/*
	TEST-Smoke-Script
	
	Author: Pazuzu
	Version: 0.1 (Initial Relase)
	
	Arguments:
		0: Object (vehicle)
		
	
	Returns:
		-
	Description:
		Proof-of-Concept script for smoke launcher.
		script in scheduled environment (therefore test purpose only)!
		

*/

params ["_vehicle"];
private ["_i","_smokeNade","_velocityVe"];

_i = 0;
while {_i < 6} do {
	_smokeNade = "G_40mm_Smoke" createVehicle [(getPos _vehicle select 0) - 0.5, getPos _vehicle select 1, 3];

	_velocityVe = velocityModelSpace _vehicle;

	_smokeNade setDir ((getDir _vehicle) + 60 - _i*20);

	if (_i == 1 || _i == 4) then {
			_smokeNade setVelocityModelSpace [0 + (_velocityVe select 0),14, 15 + (_velocityVe select 2)];
		}else{
			_smokeNade setVelocityModelSpace [0 + (_velocityVe select 0),13, 11 + (_velocityVe select 2)];
		};
	playSound3D ["A3\Sounds_F\arsenal\explosives\Grenades\Explosion_gng_grenades_01.wss", _vehicle,false,getPosASL _vehicle,1,1.4,100];
	_i = _i + 1;
	sleep 0.1;

};


_vehicle setVariable ["TnkSmokeUsed",true,true];