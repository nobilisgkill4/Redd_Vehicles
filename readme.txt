

First of all, it is not perfect, but we are getting better ;)
Secondly, our vehicles are never meant to be played with AI, we are focusing on coop content. This means, some functions may not work with AI

Redd'n'Tank Vehicles 1.2.50
18.01.2018

The Team:
Redd: Config, functions, model, textures
Tank: Model, textures

Redd'n'Tank official website:
http://reddiem.portfoliobox.net/

Discuss AW-Forum (german): 
https://armaworld.de/index.php?thread/3258-redd-vehicles-marder-1a5-release/

Discuss BI-Forum (english):
https://forums.bistudio.com/forums/topic/210296-redd-vehicles-marder-1a5-release/

Steam Workshop:
http://steamcommunity.com/sharedfiles/filedetails/?id=1128145626

YouTube:
https://www.youtube.com/playlist?list=PLJVKqjrLClHTeQcHtncGIJdRyHiTblsdT

Imgur:
https://imgur.com/a/Uv1Ei

GitLab: 
https://gitlab.com/TTTRedd/Redd_Vehicles
pls report issues here

Support us:
https://www.patreon.com/Redd_Arma_Modding

Contact:
redd.arma.modding@gmail.com

Addon Pack Contains:
- Wiesel 1A2 TOW (Requires redd_milan_static.pbo) (No AI Vehicle)
- Fuchs 1A4 Infantry (Requires redd_milan_static.pbo) (No AI Vehicle)
- Fuchs 1A4 Infantry-Milan (Requires redd_milan_static.pbo) (No AI Vehicle)
- Fuchs 1A4 Engineers (Requires redd_milan_static.pbo) (No AI Vehicle)
- Fuchs 1A4 Medic Vehicle (Requires redd_milan_static.pbo) (No AI Vehicle)
- Marder 1A5 (Requires redd_milan_static.pbo) (No AI Vehicle)
- Static Milan
- Redd Vehicles Main (Required for all other addons)

Features:
Wiesel 1A2 TOW
- Woodland, desert and winter camo
- ViV transportable
- Slingloadable
Fuchs 1A4 Infantry and Engineers
- FFV left and right rear hatch
- Actual Fuchs has two brake pedals, try the difference between normal brake (S) and handbrake (X). We tried simulate pushing both pedals with the handbrake (X)
- Mountable rotating beacon
- Woodland, desert and winter camo
- Animated doors
- Animated Surge Plate
- Animated Bullet Shield
- A few more little animations
Marder 1A5
- Rear ramp can be opened and closed by driver or rear FFV-seat
- Turned out Marder Commander can assemble Milan on turret
- FFV, rear-back-seat, left-back-seat, right-back-seat
- Commander has two stances when turned out
- Marder has Static Milan in its cargo
- Woodland, desert and winter camo
Static Milan
- Static Milan can be assembled and disassembled

Known issues:
- Some missing details in Marder interior
- Localization only english and german, all other languages display english names
- Static Milan is not accessible via Zeus

Changelog:
Version 1.2.50
- Added Wiesel 1A2 TOW
- Changed Milan static backpack now carries the ammo for static milan
- Fixed MG3 soundshader
- Fixed MK20 soundshader
- Fixed Milan soundshader
- Fixed error in Fuchs stringtable.xml
- Fixed spelling mistake in Static Milan memorypoints "pos_gunner" and "pos_gunner_dir"
Version 1.1.44
- Added simple thermal texture to Fuchs 1A4
- Removed the function to force players to open doors and hatches manually before entering a vehicle for all vehicles. Seats are no longer locked. 
  User actions to open doors or hatches are still available, so you can decide by yourself if you want to open a door first or enter a vehicle vanilla style.
- Fixed Marder can now be refueled via ACE3
- Tweaked B.O. lights for all vehicles
- Tweaked some animations
Version 1.1.38
- Addes Fuchs 1A4 Infantry Group-Milan
- Addes Fuchs 1A4 Medic Vehicle
- Added a function to let players only enter vehicles if the doors/hatches are open (player has to open doors/hatches manually. Changing seats inside while doors are closed is no longer possible)
- Added Fuchs mountable rotating beacon (Commander can assemble and disassemble while turned out, Driver and Co-Driver can turn on and off)
- Added Marder "EngineMOI" PhysX parameter 
- Fixed, set launching positions for Marder smokegrenades on the correct position
- Removed stabilization for Marder maingun
- Changed, MG3 closeShot sound
- Changed german displayname "Beschussschild" -> "Beschussklappe"
- Changed position for Marder gunner to enter the vehicle to the rear ramp (rear ramp has to be open for entering)
- Tweaked Fuchs Gearbox so it will not stuck at 30 km/h at normal acceleration (W)
- Tweaked Marder Gearbox a bit
- Tweaked Marder engine parameters a bit
Version 1.1.25
- Added Fuchs 1A4 Infantry Group
- Added Fuchs 1A4 Engineer Group
- Added new PhysX parameters to Marder
- Added some MG3 mid and distance sounds from BW-Mod (Thanks to Ironie)
- Added 500 rounds MG3 magazines
- Fixed Marder tracks can now be repaired via ACE3
- Changed, Marder maingun is now stabilized in all axes
- Changed, Marder countermeasures moved to commander 
- Changed the spelling of all folders to lower case
- Changed MG3 tracercount
- Changed Redd_smokeLauncher.sqf so it will work for vehicles without turrets
- Changed SoundShaders for MG3
- Tweaked Marder engine parameters
Version 1.0.14
- Added new Redd'n'Tank logo
- Added Marder Drivewheel animation
- Fixed issue where choosing 2nd company shows the letter 1 
- Tweaked Marder Geo LOD, GeoPhysX LOD...again, tell me if it still flips
Version 1.0.10
- Added simple thermal texture to Marder 1A5
- Changed order for all optics to, "Normal" -> "NVG" -> "TI"
- Changed materials for glass
- Tweaked Geo LOD, GeoPhysX LOD and FireGeo LOD a bit
- Tweaked PhysX.hpp now it should drive much more smoother and flips less
Version 1.0.5
- Fixed issue where crew can get killed by small arms from outside of the vehicles, now it is possible to shoot or throw grenades through the rearramp
- Fixed missing modelpart
- Fixed missing track animation
Version 1.0.2
- Added strings for Marder 1A5 Eden attributes in stringtable.xml
- Fixed some commander animation issues

License:
Licensed Under Attribution-NonCommercial-NoDerivs CC BY-NC-ND
https://creativecommons.org/licenses/by-nc-nd/4.0/

Thanks:
- Thanks to T4nk for joining me
- Thanks to Tactical Training Team and TF47 for testing and support
- Thanks to KV-13 Schubert for all background informations 
- Thanks to Pazuzu for original Fuchs config
- Thanks to commy2 for smoke launcher script
- Thanks to Ironie for MG3 sounds
- Thanks to my wife and my daughter for giving me the time to do all this stuff :* 